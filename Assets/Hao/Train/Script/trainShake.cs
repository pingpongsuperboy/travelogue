using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trainShake : MonoBehaviour
{
    public GameObject friend;
    public float offset;
    int mod;
    float percent;
    public float shakingSpeed;

    Vector3 pos1;
    Vector3 pos2;

    // Start is called before the first frame update
    void Start()
    {
        pos1 = new Vector3(transform.position.x , transform.position.y + offset, transform.position.z);
        pos2 = new Vector3(transform.position.x , transform.position.y - offset, transform.position.z);
        mod = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (friend.GetComponent<FriendShrink>().departure)
        {
            percent += shakingSpeed * Time.deltaTime * mod;
        }
        

        if (percent > 1)
        {
            mod *= -1;
            percent += shakingSpeed * Time.deltaTime * mod;
        }else if(percent < 0)
        {
            mod *= -1;
            percent += shakingSpeed * Time.deltaTime * mod;
        }

        transform.position = Vector3.Lerp(pos1, pos2 , percent);
    }
}
