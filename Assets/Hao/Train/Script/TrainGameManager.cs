using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrainGameManager : MonoBehaviour
{

    public float timeBeforeYouCanGrab;
    public bool youCanGrab = false;

    public GameObject holdText;
    
    public GameObject hand;

    public Sprite holdingHand;
    public Sprite normalHand;


    public GameObject musicPlayer;
    public GameObject friend;

    public GameObject train;
    public GameObject sideFace;
    public GameObject handHoldingStone;
    public GameObject black;

    bool grab;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("YouCanGrab", timeBeforeYouCanGrab);
    }

    // Update is called once per frame
    void Update()
    {
        


        if (youCanGrab == true && Input.GetMouseButtonDown(0))
        {
            hand.GetComponent<SpriteRenderer>().sprite = holdingHand;

            if (hand.transform.rotation.eulerAngles.z > 38 && hand.transform.rotation.eulerAngles.z < 42)
            {
                holdText.SetActive(false);
                musicPlayer.SetActive(false);
                friend.SetActive(false);
                grab = true;
                Invoke("ending01", 4f);

            }

        }

        if (Input.GetMouseButtonUp(0) && grab==false)
        {
            hand.GetComponent<SpriteRenderer>().sprite = normalHand;
        }


    }

    void YouCanGrab()
    {
        youCanGrab = true;
        holdText.SetActive(true);

    }


    void ending01()
    {
        train.SetActive(false);
        sideFace.SetActive(true);
        Invoke("ending02", 3f);

    }

    void ending02()
    {
        hand.SetActive(false);
        sideFace.SetActive(false);
        handHoldingStone.SetActive(true);
        Invoke("cutToBlack", 5f);

    }

    void cutToBlack()
    {
        black.SetActive(true);
        Invoke("BackToMenu", 3f);
        
    }

    void BackToMenu()
    {
        SceneManager.LoadScene("MagicStone");
    }


}
