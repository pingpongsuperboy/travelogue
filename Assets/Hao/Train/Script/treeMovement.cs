using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class treeMovement : MonoBehaviour
{
    public GameObject friend;
    public SpriteRenderer myRender;
    public Color color;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //scale
        transform.localScale = new Vector3(map(transform.position.x, 0, 10, 0, 2),
            map(transform.position.x, 0, 10, 0, 12),
            1
            );
        //alpha
        color.a = map(transform.position.x, 1, 10, 0, 1);
        myRender.color = color;


        if (friend.GetComponent<FriendShrink>().departure)
        {
            if (transform.position.x <= 0)
            {
                transform.position = new Vector3(10, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            
        }

        

    }

    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }
}
