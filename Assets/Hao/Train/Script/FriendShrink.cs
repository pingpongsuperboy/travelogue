using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendShrink : MonoBehaviour
{
    public float speed;
    public float departureTime;
    public Vector2 finalScale;

    public bool departure;

    // Start is called before the first frame update
    void Start()
    {
        departure = false;
        Invoke("Departure", departureTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (departure)
        {
            transform.localScale = Vector2.Lerp(transform.localScale, finalScale, speed * Time.deltaTime);
        }
        
    }

    void Departure()
    {
        departure = true;
    }


}
