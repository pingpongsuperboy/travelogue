using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ItemMenuManager : MonoBehaviour
{

    public int howManyGamesArePlayed;
    public bool kite;
    public bool tooth;
    public bool postCard;
    public bool coin;
    public bool hat;
    /*public GameObject kiteObject;
    public GameObject toothObject;
    public GameObject postCardObject;
    public GameObject coinObject;
    public GameObject hatObject;*/

    public GameObject[] objs;
    public bool check;

    private static ItemMenuManager instance;

    //in some cases, if a variable of this game manager is NOT static, we'll need a reference to
    //the instance with the game manager

    public static ItemMenuManager FindInstance()
    {
        return instance;
    }

    private void Awake()
    {
        //if we have already chosen a king game manager
        //(null literally means "nothing", so if instance is NOT nothing)
        //AND
        //if the king game manager is NOT this instance of the class
        //destroy this
        if (instance != null && instance != this)
        {
            //what we;re doing is ensuring that there can only
            //be one game manager in any scene
            Destroy(this);
        }
        else //otherwise, if we do not have a king game manager
        {
            //make this the king game manager
            instance = this;
            //and do not destroy this game object when we load new scenes
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        howManyGamesArePlayed = 0;

       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Item");
            
            Invoke("Check", 0.5f);
        }
        
        /*if (!check) {
            Check();
        }*/

    }


    void Check()
    {
        objs = FindObjectsOfType<GameObject>();

        if (kite == true)
        {
            objs[4].SetActive(false);
        }

        if (tooth == true)
        {
            objs[9].SetActive(false);
        }
        if (hat == true)
        {
            objs[6].SetActive(false);
        }
        if (postCard == true)
        {
            objs[2].SetActive(false);
        }
        if (coin == true)
        {
            objs[10].SetActive(false);
        }

        check = true;
    }



}
