using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ItemMenuPickItem : MonoBehaviour
{
    public Text descriptionText;
    public string[] description;
    public string SCENE_NAME;
    public bool isKite;
    public bool isTooth;
    public bool isCoin;
    public bool isHat;
    public bool isPostCard;
    //ItemMenuManager myManager = FindObjectOfType<ItemMenuManager>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter()
    {
        ItemMenuManager myManager = FindObjectOfType<ItemMenuManager>();
        descriptionText.text = description[myManager.howManyGamesArePlayed];
    }

    private void OnMouseExit()
    {
        descriptionText.text = "I lost these things on the trip.";
    }

    private void OnMouseDown()
    {
        ItemMenuManager myManager = FindObjectOfType<ItemMenuManager>();
        myManager.howManyGamesArePlayed++;
        if (isKite)
        {
            myManager.kite = true;
        }
        if (isHat)
        {
            myManager.hat = true;
        }
        if (isCoin)
        {
            myManager.coin = true;
        }
        if (isTooth)
        {
            myManager.tooth = true;
        }
        if (isPostCard)
        {
            myManager.postCard = true;
        }
        
        descriptionText.text = "I lost these things on the trip.";
        SceneManager.LoadScene(SCENE_NAME);
        
    }


}
