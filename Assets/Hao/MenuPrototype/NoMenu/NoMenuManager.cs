using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NoMenuManager : MonoBehaviour
{
    public string[] gameTitle;
    public int[] sceneNumber;
    int index;

    private static NoMenuManager instance;

    //in some cases, if a variable of this game manager is NOT static, we'll need a reference to
    //the instance with the game manager

    public static NoMenuManager FindInstance()
    {
        return instance;
    }

    private void Awake()
    {
        //if we have already chosen a king game manager
        //(null literally means "nothing", so if instance is NOT nothing)
        //AND
        //if the king game manager is NOT this instance of the class
        //destroy this
        if (instance != null && instance != this)
        {
            //what we;re doing is ensuring that there can only
            //be one game manager in any scene
            Destroy(this);
        }
        else //otherwise, if we do not have a king game manager
        {
            //make this the king game manager
            instance = this;
            //and do not destroy this game object when we load new scenes
            DontDestroyOnLoad(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        Shuffle(sceneNumber);
        Text title = FindObjectOfType<Text>();
        title.text = "Press ESC to travel to next place.";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("NoMenu");
            //ChangeGame();
            Invoke("ChangeGame", 0.1f);

        }
    }

    void ChangeGame()
    {
        Text title = FindObjectOfType<Text>();
        title.text = gameTitle[sceneNumber[index]];
        
        Invoke("LoadNextGame", 3f);

    }

    void LoadNextGame()
    {
        SceneManager.LoadScene(sceneNumber[index]);
        index++;
    }


    void Shuffle(int[] a)
    {
        // Loops through array
        for (int i = a.Length - 1; i > 0; i--)
        {
            // Randomize a number between 0 and i (so that the range decreases each time)
            int rnd = Random.Range(0, i);

            // Save the value of the current i, otherwise it'll overright when we swap the values
            int temp = a[i];

            // Swap the new and old values
            a[i] = a[rnd];
            a[rnd] = temp;
        }

        // Print
        for (int i = 0; i < a.Length; i++)
        {
            Debug.Log(a[i]);
        }
    }

}
