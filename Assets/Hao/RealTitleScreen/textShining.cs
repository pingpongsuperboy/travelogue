using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textShining : MonoBehaviour
{
    Text myText;
    Color myTextColor = Color.white;

    public GameObject menuManager;
    public float shiningSpeed;
    public float minAlpha;
    float angle;

    public GameObject portalEnlarge;
    public GameObject menuBasic;
    public float timeAfterPressed;
    bool keyPressed;


    // Start is called before the first frame update
    void Start()
    {
        myText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        myTextColor.a =map( Mathf.Sin(angle),-1,1, minAlpha, 1);
        myText.color = myTextColor;
        angle += shiningSpeed*Time.deltaTime;


        //press any button
        if (Input.anyKeyDown && !keyPressed)
        {
            // Debug.Log("A key or mouse click has been detected");
            
            Invoke("anyKeyPressed", timeAfterPressed);
            shiningSpeed = 50;
            keyPressed = true;

        }


    }

    void anyKeyPressed()
    {
        portalEnlarge.SetActive(true);
        Invoke("displayMenu", 0.5f);
        myText.text = " ";
    }

    void displayMenu()
    {
        menuBasic.SetActive(true);
        menuManager.SetActive(true);
    }


    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }
}
