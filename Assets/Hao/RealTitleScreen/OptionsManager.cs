using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public MenuManager myMenuManager;
    public GameObject BasicMenu;
    public GameObject optionsSection;
    public GameObject frame;
    public GameObject triangleForVol;
    public GameObject squareNoFill;
    public GameObject squareFill;
    public GameObject triangleForLanguage;

    public GameObject english;
    public GameObject japanese;

    public SpriteRenderer startSprite;
    public SpriteRenderer optionsSprite;
    public SpriteRenderer creditsSprite;
    public SpriteRenderer exitSprite;
    public SpriteRenderer japaneseSprite;

    public Text volNum;
    public int volume = 50;

    Color myTextColor = Color.white;
    public float shiningSpeed;
    public float minAlpha;
    float angle;

    public Transform startPos;
    public Transform optionPos;
    public Transform creditPos;
    public Transform exitPos;

    public float speed;
    public float step;

    bool startToOptions;
    bool optionsToStart;
    bool optionsToCredits;
    bool creditsToOptions;
    bool creditsToExit;
    bool exitToCredits;
    bool exitToStart;
    bool startToExit;
    public float timeAfterPressed;
    bool switchable;

    public State currentState;

    public enum State
    {
        Start,
        Options,
        Credits,
        Exit

    }


    // Start is called before the first frame update
    void Start()
    {
        frame.transform.position = startPos.position;
        currentState = State.Start;
        switchable = true;

    }

    // Update is called once per frame
    void Update()
    {
        volNum.text = volume.ToString()+ "%";
        myTextColor.a = map(Mathf.Sin(angle), -1, 1, minAlpha, 1);
        angle += shiningSpeed * Time.deltaTime;
        step = speed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.DownArrow) && switchable)
        {
            if (currentState == State.Start)
            {
                startToOptions = true;
            }

            if (currentState == State.Options)
            {
                optionsToCredits = true;
            }

            if (currentState == State.Credits)
            {
                creditsToExit = true;
            }

            if (currentState == State.Exit)
            {
                exitToStart = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && switchable)
        {
            if (currentState == State.Start)
            {
                startToExit = true;
            }

            if (currentState == State.Options)
            {
                optionsToStart = true;
            }

            if (currentState == State.Credits)
            {
                creditsToOptions = true;
            }

            if (currentState == State.Exit)
            {
                exitToCredits = true;
            }
        }


        if (startToOptions)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, optionPos.position, step);

            if (Vector3.Distance(frame.transform.position, optionPos.position) < 0.01f)
            {
                startToOptions = false;
                currentState = State.Options;
            }

        }

        if (optionsToCredits)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, creditPos.position, step);
            if (Vector3.Distance(frame.transform.position, creditPos.position) < 0.01f)
            {
                optionsToCredits = false;
                currentState = State.Credits;
            }
        }

        if (creditsToExit)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, exitPos.position, step);
            if (Vector3.Distance(frame.transform.position, exitPos.position) < 0.01f)
            {
                creditsToExit = false;
                currentState = State.Exit;
            }
        }

        if (exitToStart)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, startPos.position, step * 2);
            if (Vector3.Distance(frame.transform.position, startPos.position) < 0.01f)
            {
                exitToStart = false;
                currentState = State.Start;
            }
        }

        if (startToExit)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, exitPos.position, step * 2);
            if (Vector3.Distance(frame.transform.position, exitPos.position) < 0.01f)
            {
                startToExit = false;
                currentState = State.Exit;
            }
        }

        if (optionsToStart)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, startPos.position, step);
            if (Vector3.Distance(frame.transform.position, startPos.position) < 0.01f)
            {
                optionsToStart = false;
                currentState = State.Start;
                
            }
        }

        if (creditsToOptions)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, optionPos.position, step);
            if (Vector3.Distance(frame.transform.position, optionPos.position) < 0.01f)
            {
                creditsToOptions = false;
                currentState = State.Options;
            }
        }

        if (exitToCredits)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, creditPos.position, step);
            if (Vector3.Distance(frame.transform.position, creditPos.position) < 0.01f)
            {
                exitToCredits = false;
                currentState = State.Credits;
            }
        }

        switch (currentState)
        {
            case State.Start:
                //text shining
                volNum.color = myTextColor;
                startSprite.color = myTextColor;
                optionsSprite.color = Color.white;
                creditsSprite.color = Color.white;
                exitSprite.color = Color.white;
                japaneseSprite.color = Color.white;
                triangleForVol.SetActive(true);
                triangleForLanguage.SetActive(false);
                squareNoFill.SetActive(false);

                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    if (volume >= 100)
                    {

                    }
                    else
                    {
                        volume += 10;
                    }
                }
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    if (volume <= 0)
                    {

                    }
                    else
                    {
                        volume -= 10;
                    }
                }

                //select

                break;
            case State.Options:
                //text shining
                optionsSprite.color = myTextColor;
                startSprite.color = Color.white;
                creditsSprite.color = Color.white;
                exitSprite.color = Color.white;
                volNum.color = Color.white;
                japaneseSprite.color = Color.white;
                triangleForVol.SetActive(false);
                triangleForLanguage.SetActive(false);
                squareNoFill.SetActive(true);

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    if (squareFill.activeSelf)
                    {
                        squareFill.SetActive(false);
                    }else if (!squareFill.activeSelf)
                    {
                        squareFill.SetActive(true);
                    }
                }
                break;

            case State.Credits:
                //text shining
                creditsSprite.color = myTextColor;
                japaneseSprite.color = myTextColor;
                optionsSprite.color = Color.white;
                startSprite.color = Color.white;
                exitSprite.color = Color.white;
                volNum.color = Color.white;
                triangleForVol.SetActive(false);
                triangleForLanguage.SetActive(true);
                squareNoFill.SetActive(false);

                //select
                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    if (english.activeSelf)
                    {
                        english.SetActive(false);
                        japanese.SetActive(true);
                    }else if (japanese.activeSelf)
                    {
                        english.SetActive(true);
                        japanese.SetActive(false);
                    }

                }
                

                break;

            case State.Exit:
                //text shining
                exitSprite.color = myTextColor;
                optionsSprite.color = Color.white;
                creditsSprite.color = Color.white;
                startSprite.color = Color.white;
                volNum.color = Color.white;
                japaneseSprite.color = Color.white;
                triangleForVol.SetActive(false);
                triangleForLanguage.SetActive(false);
                squareNoFill.SetActive(false);

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    shiningSpeed = 50;
                    Invoke("GoBack", timeAfterPressed);
                    switchable = false;
                }

                break;


            

        }



    }


    void GoBack()
    {
        optionsSection.SetActive(false);
        BasicMenu.SetActive(true);
        //myMenuManager.currentState= MenuManager.State.Options;
        shiningSpeed = 5;
        currentState = State.Start;
        frame.transform.position = startPos.position;
        switchable = true;
    }

    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }
}
