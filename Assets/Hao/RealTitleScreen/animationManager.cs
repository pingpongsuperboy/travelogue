using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationManager : MonoBehaviour
{

    public Sprite[] mySprites;
    //public TitleScreenManager myManager;
    public float timeGap;
    int index;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        GetComponent<SpriteRenderer>().sprite = mySprites[index];
        InvokeRepeating("ChangeToNextFrame", timeGap, timeGap);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ChangeToNextFrame()
    {
        if (mySprites.Length == 4)
        {
            //Debug.Log("changed");
            if (index == 3)
            {
                index = 0;
                GetComponent<SpriteRenderer>().sprite = mySprites[index];
            }
            else
            {
                index++;
                GetComponent<SpriteRenderer>().sprite = mySprites[index];
            }
        }


        if (mySprites.Length == 9)
        {
            //Debug.Log("changed");
            if (index == 8)
            {
                
            }
            else
            {
                index++;
                GetComponent<SpriteRenderer>().sprite = mySprites[index];
            }
        }


    }

}
