using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject BasicMenu;
    public GameObject frame;
    public GameObject start;
    public GameObject options;
    public GameObject credits;
    public GameObject exit;
    public GameObject portalEnlarge;
    public GameObject portalShrink;
    public GameObject fadeToWhite;
    public GameObject title;
    public GameObject creditSection;
    public GameObject optionsSection;


    public SpriteRenderer startSprite;
    public SpriteRenderer optionsSprite;
    public SpriteRenderer creditsSprite;
    public SpriteRenderer exitSprite;

    public SpriteRenderer ruturnSprite;

    Color myTextColor = Color.white;
    public float shiningSpeed;
    public float minAlpha;
    float angle;

    public Transform startPos;
    public Transform optionPos;
    public Transform creditPos;
    public Transform exitPos;

    public float speed;
    public float step;
    public GameObject goToMenuObject;

    

    bool startToCredits;
    bool creditsToStart;
    //bool optionsToCredits;
    //bool creditsToOptions;
    bool creditsToExit;
    bool exitToCredits;
    bool exitToStart;
    bool startToExit;
    public float timeAfterPressed;
    bool switchable;

    public State currentState;
    public enum State
    {
        Start,
        //Options,
        Credits,
        Exit,
        OptionsSelected,
        CreditsSelected

    }

    public bool gameStart;

    // Start is called before the first frame update
    void Start()
    {
        frame.transform.position = startPos.position;
        currentState = State.Start;
        switchable = true;
    }

    // Update is called once per frame
    void Update()
    {
        myTextColor.a = map(Mathf.Sin(angle), -1, 1, minAlpha, 1);
        angle += shiningSpeed * Time.deltaTime;

        step = speed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.DownArrow) && switchable)
        {
            if (currentState == State.Start)
            {
                startToCredits = true;
            }

            if (currentState == State.Credits)
            {
                creditsToExit = true;
            }

            if (currentState == State.Exit)
            {
                exitToStart = true;
            }


        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && switchable)
        {
            if (currentState == State.Start)
            {
                startToExit = true;
            }

            if (currentState == State.Credits)
            {
                creditsToStart = true;
            }

            if (currentState == State.Exit)
            {
                exitToCredits = true;
            }

            
        }


        if (startToCredits)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, creditPos.position, step);

            if (Vector3.Distance(frame.transform.position, creditPos.position) < 0.01f)
            {
                startToCredits = false;
                currentState = State.Credits;
            }

        }

        /*if (optionsToCredits)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, creditPos.position, step);
            if (Vector3.Distance(frame.transform.position, creditPos.position) < 0.01f)
            {
                optionsToCredits = false;
                currentState = State.Credits;
            }
        }*/

        /*if (creditsToStart)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, exitPos.position, step);
            if (Vector3.Distance(frame.transform.position, exitPos.position) < 0.01f)
            {
                creditsToStart = false;
                currentState = State.Start;
            }
        }*/

        if (exitToStart)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, startPos.position, step*2);
            if (Vector3.Distance(frame.transform.position, startPos.position) < 0.01f)
            {
                exitToStart = false;
                currentState = State.Start;
            }
        }

        if (startToExit)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, exitPos.position, step*2);
            if (Vector3.Distance(frame.transform.position, exitPos.position) < 0.01f)
            {
                startToExit = false;
                currentState = State.Exit;
            }
        }

        if (creditsToStart)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, startPos.position, step);
            if (Vector3.Distance(frame.transform.position, startPos.position) < 0.01f)
            {
                creditsToStart = false;
                currentState = State.Start;
            }
        }

        if (creditsToExit)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, exitPos.position, step);
            if (Vector3.Distance(frame.transform.position, exitPos.position) < 0.01f)
            {
                creditsToExit = false;
                currentState = State.Exit;
            }
        }

        if (exitToCredits)
        {
            frame.transform.position = Vector3.MoveTowards(frame.transform.position, creditPos.position, step);
            if (Vector3.Distance(frame.transform.position, creditPos.position) < 0.01f)
            {
                exitToCredits = false;
                currentState = State.Credits;
            }
        }


        
      


        switch (currentState)
        {
            case State.Start:
                //text shining
                startSprite.color = myTextColor;
                optionsSprite.color = Color.white;
                creditsSprite.color = Color.white;
                exitSprite.color = Color.white;

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    shiningSpeed = 50;
                    Invoke("changeToAnotherSection", timeAfterPressed);
                    switchable = false;

                    //put Game Start function here !!!!!!!!!!!!!!
                    goToMenuObject.SetActive(true);


                }
                break;
           /* case State.Options:
                //text shining
                optionsSprite.color = myTextColor;
                startSprite.color = Color.white;
                creditsSprite.color = Color.white;
                exitSprite.color = Color.white;

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    shiningSpeed = 50;
                    Invoke("changeToAnotherSection", timeAfterPressed);
                    switchable = false;
                }
                break;*/

            case State.Credits:
                //text shining
                creditsSprite.color = myTextColor;
                optionsSprite.color = Color.white;
                startSprite.color = Color.white;
                exitSprite.color = Color.white;
                

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    shiningSpeed = 50;
                    Invoke("changeToAnotherSection", timeAfterPressed);
                    switchable = false;
                }
                break;

            case State.Exit:
                //text shining
                exitSprite.color = myTextColor;
                optionsSprite.color = Color.white;
                creditsSprite.color = Color.white;
                startSprite.color = Color.white;

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    shiningSpeed = 50;
                    Invoke("changeToAnotherSection", timeAfterPressed);
                    switchable = false;

                    //put Quit GAME function here !!!!!!!!!!!!!!
                    Application.Quit();

                }

                break;


            case State.CreditsSelected:
                ruturnSprite.color = myTextColor;

                //select
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
                {
                    shiningSpeed = 50;
                    Invoke("changeToAnotherSection", timeAfterPressed);
                    
                }

                break;

        }



    }

    void changeToAnotherSection()
    {
        BasicMenu.SetActive(false);
        shiningSpeed = 5;
        switchable = true;
        switch (currentState)
        {
            case State.Start:
                portalEnlarge.SetActive(false);
                portalShrink.SetActive(true);
                
                Invoke("GameStart", 0.65f);
                break;
            /*case State.Options:
                currentState = State.OptionsSelected;
                optionsSection.SetActive(true);
                break;*/
            case State.Credits:
                currentState = State.CreditsSelected;
                creditSection.SetActive(true);
                title.SetActive(false);
                break;
            case State.Exit:
                break;
            case State.CreditsSelected:
                currentState = State.Credits;
                creditSection.SetActive(false);
                BasicMenu.SetActive(true);
                title.SetActive(true);
                break;
            case State.OptionsSelected:
                
                break;
        }
        
    }

    void GameStart()
    {
        gameStart = true;
        Invoke("FadeToWhite", 0.9f);
    }

    void FadeToWhite()
    {
        fadeToWhite.SetActive(true);
    }

    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }
}
