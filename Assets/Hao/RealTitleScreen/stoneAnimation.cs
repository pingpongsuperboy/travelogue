using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stoneAnimation : MonoBehaviour
{
    public MenuManager myMenuManager;
    public float percent;
    public float percent2;
    public float percent3;
    float mod = 1;
    float mod2 = 1;
    float originalYPos;
    public float offset;
    float randomizeSpeed;


    public Transform target;

    void Start()
    {
        originalYPos = transform.position.y;
        randomizeSpeed = Random.Range(1, 3);
        
    }

    
    void Update()
    {

        percent += Time.deltaTime * mod/randomizeSpeed;
        percent2 += Time.deltaTime * mod2/randomizeSpeed/3;

        if (percent <= 0 || percent > 1) // make this animation loop
        {
            mod *= -1;
            percent += Time.deltaTime * mod;
        }

        if (percent2 <= 0 || percent2 > 1) // make this animation loop
        {
            mod2 *= -1;
            percent2 += Time.deltaTime * mod2/2;
        }

        // use Lerp to make this item move up and down
        if (!myMenuManager.gameStart)
        {
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, originalYPos, transform.position.z), new Vector3(transform.position.x, originalYPos + offset, transform.position.z), percent);
            transform.eulerAngles = Vector3.Lerp(new Vector3(0, 0, 6), new Vector3(0, 0, -6), percent2);
        }
        else
        {
            percent3 += Time.deltaTime * 0.075f;
            transform.position = Vector3.Lerp(transform.position, target.position, percent3);
        }

        

        
       

    }
}
