using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateManager : MonoBehaviour
{
    public float xAmplitude;
    public float yAmplitude;
    public float frequency;
    public float rotateSpeed;
    public float centerX;
    public float centerY;
    public float rise;
    public float angle;
    public float[] stoneAngleMod;

    public GameObject stones;
    public GameObject beam;
    public GameObject fadeToWhite;
    public Vector3 originalPos;
    public Vector3 destination;
    public bool stopRotating;
    public float offset; 
    float timer;

    public float[] timing;

    void Start()
    {
        frequency = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //use this to fade to white
        //fadeToWhite.SetActive(true);

        timer += Time.deltaTime;

        if (timer > timing[4])
        {
            beam.SetActive(true);
            stopRotating = true;

        }
        else if (timer > timing[3])
        {
            if (xAmplitude >= 0.5f)
            {
                xAmplitude -= Time.deltaTime;
                yAmplitude -= Time.deltaTime;
            }
            if (frequency <= 4f)
            {
                frequency += Time.deltaTime;
            }



        }
        else  if(timer > timing[2])
        {
            

            if (xAmplitude >= 1.15f)
            {
                xAmplitude -= Time.deltaTime ;
            }
            if (frequency <= 2.5f)
            {
                frequency += Time.deltaTime ;
            }

        } else if (timer > timing[1])
        {
            /*angle = 0.9f;
            stoneAngleMod[1] = 1;
            stoneAngleMod[2] = 2;
            stoneAngleMod[3] = 3;
            stoneAngleMod[4] = 4;
            stoneAngleMod[5] = 5;
            stoneAngleMod[6] = 6;*/
            if (frequency <= 1)
            {
                frequency += Time.deltaTime/3;
            }

        }else if (timer > timing[0])
        {
            if (rise <= 2.65)
            {
                rise += Time.deltaTime;
            }
            else
            {
                frequency = 0.5f;
            }
        }

        



    }
}
