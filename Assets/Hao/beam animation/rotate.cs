using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    public rotateManager myManager;
    public int index;

    public Vector3 destination;
    float step;
    
    public bool makingWish;
    float percent;
    int mod=1;
    // Start is called before the first frame update
    void Start()
    {
        


    }

    // Update is called once per frame
    void Update()
    {
        if (makingWish)
        {
            percent += Time.deltaTime * mod;
            if (percent <= 0 || percent >= 1) // make this animation loop
            {
                mod *= -1;
                percent += Time.deltaTime * mod;
            }
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, destination.y, transform.position.z), new Vector3(transform.position.x, destination.y + myManager.offset, transform.position.z), percent);
            //transform.eulerAngles = Vector3.Lerp(new Vector3(0, 0, 6), new Vector3(0, 0, -6), percent);
        }
        else
        {
            if (!myManager.stopRotating)
            {
                myManager.rotateSpeed += myManager.frequency * Time.deltaTime; //rotate speed

                float x = myManager.centerX + Mathf.Cos(myManager.rotateSpeed + myManager.stoneAngleMod[index] * myManager.angle) * myManager.xAmplitude; // use sin and cos to make the object rotate
                float y = myManager.centerY + myManager.rise + Mathf.Sin(myManager.rotateSpeed + myManager.stoneAngleMod[index] * myManager.angle) * myManager.yAmplitude;
                float z = transform.position.z;

                transform.position = new Vector3(x, y, z);
            }
            else
            {
                step += Time.deltaTime;
                transform.position = Vector3.Lerp(transform.position, destination, step * 2);
                if (Vector3.Distance(transform.position, destination) < 0.05f)
                {
                    makingWish = true;
                }

            }
        }


        
        


    }
}
