using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MagicStoneGameManager : MonoBehaviour
{
    public float introSpeed;
    public GameObject text01;
    public GameObject text02;
    public GameObject text03;
    public GameObject image;
    public GameObject buttons;

    public GameObject introGameobject;

    public GameObject frined01;
    public GameObject frined02;
    public GameObject frined03;

    public GameObject characterSelectionOtherStuff;

    public GameObject selectionMenu;
    public GameObject tutorial;
    public GameObject quitGame;

    int intro = 0;

    public static bool haveSeenTheTutorial = false;

    public static bool playedTrainGame = false;
    public static bool playedCardGame = false;
    public static bool playedKiteGame = false;

    public GameObject trainGameAssetInTheMenu;
    public GameObject cardGameAssetInTheMenu;
    public GameObject kiteGameAssetInTheMenu;


    private static MagicStoneGameManager instance;

    //in some cases, if a variable of this game manager is NOT static, we'll need a reference to
    //the instance with the game manager

    public static MagicStoneGameManager FindInstance()
    {
        return instance;
    }

    private void Awake()
    {/*
      * 
      * 
        //if we have already chosen a king game manager
        //(null literally means "nothing", so if instance is NOT nothing)
        //AND
        //if the king game manager is NOT this instance of the class
        //destroy this
        if (instance != null && instance != this)
        {
            //what we;re doing is ensuring that there can only
            //be one game manager in any scene
            Destroy(this);
        }
        else //otherwise, if we do not have a king game manager
        {
            //make this the king game manager
            instance = this;
            //and do not destroy this game object when we load new scenes
            DontDestroyOnLoad(gameObject);
        }
    */
        }

    // Start is called before the first frame update
    void Start()
    {
        

        if (haveSeenTheTutorial)
        {
            introGameobject.SetActive(false);
            image.SetActive(true);
            buttons.SetActive(true);
        }
        else
        {
            introText();
        }

    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MagicStone");

        }*/


        if (quitGame.activeSelf && Input.GetMouseButtonDown(0))
        {
            quitGame.SetActive(false);

        }

        if (tutorial.activeSelf && Input.GetMouseButtonDown(0))
        {
            tutorial.SetActive(false);
            frined01.SetActive(true);
            characterSelectionOtherStuff.SetActive(true);
        }

        if (playedTrainGame)
        {
            trainGameAssetInTheMenu.SetActive(true);
            //Debug.Log("playedTrainGame");
        }
        if (playedCardGame)
        {
            cardGameAssetInTheMenu.SetActive(true);

        }
        if (playedKiteGame)
        {
            kiteGameAssetInTheMenu.SetActive(true);
        }

    }

    void introText()
    {
        intro++;
        
        if(intro == 1)
        {
            text01.SetActive(true);
        }
        if (intro == 2)
        {
            text01.SetActive(false);
            text02.SetActive(true);
        }
        if (intro == 3)
        {
            text02.SetActive(false);
            text03.SetActive(true);
        }

        if (intro == 4)
        {
            text03.SetActive(false);
            image.SetActive(true);
            buttons.SetActive(true);
        }
        
        if(intro > 4)
        {

        }
        else
        {
            Invoke("introText", introSpeed);
        }
            
        
    }

    public void StartTutorial()
    {
        if (!haveSeenTheTutorial)
        {
            selectionMenu.SetActive(true);
            tutorial.SetActive(true);
            haveSeenTheTutorial = true;
        }
        else
        {
            frined01.SetActive(true);
            selectionMenu.SetActive(true);
            characterSelectionOtherStuff.SetActive(true);
        }
        

    }

    public void QuitGame()
    {
        quitGame.SetActive(true);
    }

    public void nextTraveler()
    {
        if (frined01.activeSelf)
        {
            frined01.SetActive(false);
            frined02.SetActive(true);
            //
        }else if(frined02.activeSelf)
        {
            frined02.SetActive(false);
            frined03.SetActive(true);
        }
        else if (frined03.activeSelf)
        {
            frined03.SetActive(false);
            frined01.SetActive(true);
        }


    }


    public void PlayGames()
    {
        if (frined01.activeSelf)
        {
            selectionMenu.SetActive(false);
            frined01.SetActive(false);
            characterSelectionOtherStuff.SetActive(false);
            playedTrainGame = true;
            SceneManager.LoadScene("Train");
        }
        else if (frined02.activeSelf)
        {
            selectionMenu.SetActive(false);
            frined02.SetActive(false);
            characterSelectionOtherStuff.SetActive(false);
            playedCardGame = true;
            SceneManager.LoadScene("CardGame");
        }
        else if (frined03.activeSelf)
        {
            selectionMenu.SetActive(false);
            frined03.SetActive(false);
            characterSelectionOtherStuff.SetActive(false);
            playedKiteGame = true;
            SceneManager.LoadScene("Kite Game");
        }


    }



}
