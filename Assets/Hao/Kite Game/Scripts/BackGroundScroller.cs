using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScroller : MonoBehaviour
{

    public KiteGameManager myManager;
    public float scrollSpeed;

    public bool isPast;

    float offset;
    Material mat;
    public bool isCloud;
    public bool animated;
    public bool isSea;
    public bool isSun;
    public bool isGrass;
    public bool isFrontGrass;
    public float animationSpeed;
    int animationIndex=0;
    public int index;
    /*
    public Material[] material;
    int animationIndex = 0;*/

    // Start is called before the first frame update
    void Start()
    {
        
        mat = GetComponent<Renderer>().material;
       if (animated)
        {
          
                InvokeRepeating("Animation", 0, animationSpeed);
           
                
        }
    }

    // Update is called once per frame
    void Update()
    {
        //exploration
        if(MovingFromHotel.arrive && myManager.runningGame == false && isPast == false)
        {
            offset += (Time.deltaTime * scrollSpeed) / 10f * ExplorationManager.walkingBGspeed ;
        }

        if(MovingFromHotel.arrive && myManager.runningGame == false && isPast == true)
        {
            offset += (Time.deltaTime * scrollSpeed) / 10f * ExplorationManager.PastwalkingBGspeed;
        }


        //running
        if (isPast == true)
        {
            //myManager.gameStart == true
            if (myManager.runningGame == true)
            {
                if (isCloud)
                {
                    offset += (Time.deltaTime * scrollSpeed) / 10f + ((Time.deltaTime * scrollSpeed) / 10f * KiteGameManager.scrollSpeedMod / 2 * KiteGameManager.stopRunningMod);
                }
                else
                {
                    offset += (Time.deltaTime * scrollSpeed) / 10f * KiteGameManager.scrollSpeedMod * KiteGameManager.stopPastBGforEnding*KiteGameManager.stopRunningMod;
                }
                    
            }

            
        }
        else
        {
            if (isCloud)
            {
                offset += (Time.deltaTime * scrollSpeed) / 10f + (Time.deltaTime * scrollSpeed) / 10f * KiteGameManager.scrollSpeedMod / 2 * KiteGameManager.stopRunningMod;
            }
            else
            {
                offset += (Time.deltaTime * scrollSpeed) / 10f * KiteGameManager.scrollSpeedMod * KiteGameManager.stopRunningMod;
            }
        }
        
        mat.SetTextureOffset("_MainTex", new Vector2(offset, 0));

    }


    void Animation()
    {
        if (isPast)
        {
            if (animationIndex == 0)
            {
                if (index == 0)
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -3f);
                    }
                }
                else
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 1;
            }
            else if (animationIndex == 1)
            {
                if (index == 1)
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -3f);
                    }
                }
                else
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 2;
            }
            else if (animationIndex == 2)
            {
                if (index == 2)
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -3f);
                    }
                }
                else
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 3;
            }
            else if (animationIndex == 3)
            {
                if (index == 3)
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -3f);
                    }
                }
                else
                {
                    if (isGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                    if (isFrontGrass)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 0;
            }


        }
        else
        {
            if (animationIndex == 0)
            {
                if (index == 0)
                {
                    if (isSea)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isSun)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1.1f);
                    }
                }
                else
                {
                    if (isSea)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -0.8f);
                    }
                    if (isSun)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 1;
            }
            else if (animationIndex == 1)
            {
                if (index == 1)
                {
                    if (isSea)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isSun)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1.1f);
                    }
                }
                else
                {
                    if (isSea)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -0.8f);
                    }
                    if (isSun)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 2;
            }
            else if (animationIndex == 2)
            {
                if (index == 2)
                {
                    if (isSea)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
                    }
                    if (isSun)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -1.1f);
                    }
                }
                else
                {
                    if (isSea)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, -0.8f);
                    }
                    if (isSun)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 10f);
                    }
                }
                animationIndex = 0;
            }





        }




        /*
        if (animationIndex == 0)
        {
            Debug.Log("123");
            gameObject.GetComponent<Renderer>().material = material[0];
            animationIndex = 1;
        }
        else if (animationIndex==1)
        {
            gameObject.GetComponent<Renderer>().material = material[1];
            animationIndex = 2;
        }
        else if (animationIndex==2)
        {
            gameObject.GetComponent<Renderer>().material = material[2];
            animationIndex = 0;
        }*/

    }

}
