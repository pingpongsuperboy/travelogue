using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KiteGameManager : MonoBehaviour
{

    [Header("Game State")]
    public  bool gameStart;
    public bool gameEnd;
    public bool sayGoodbye;
    public bool runningGame;

    [Header("Split Screen")]
    public Camera presentCam;
    public Camera pastCam;

    public float presentCamX;
    public float pastCamX;

    int mod = 1;


    [Header("Animation")]
    public static float scrollSpeedMod;
    public static int stopRunningMod;
    public static bool stopAnimation;
    public static float stopPastBGforEnding;
    public float memorySpeed;
    public GameObject pastChar;
    public GameObject presentChar;
    public GameObject KiteString;
    public GameObject instruction;

    public Transform fallenRotation;
    public Transform standUpRotation;

    public GameObject FinalDialogue01;
    public GameObject FinalDialogue02;
    public GameObject Final;


    [Header("Pressing Experience")]

    public int[] pressCountSection;
    [Tooltip("The amount of speed change between each section")]
    public float accelerate;
    [Tooltip("How many progression every press can make")]
    public float pressSpeed;

    public static float pressCount;
    public float stopRunningCountDown;

    public float pressCountMinusMod;

    int countToThreeToCancelInstruction;


    public float goodByeCountDown;
    bool letGo;

    public GameObject BGMPlayer;
    bool standup;


    bool watchedEnding = false;

    bool kiteFlyAway = false;
    public GameObject kite;

    // Start is called before the first frame update
    void Start()
    {
        
        countToThreeToCancelInstruction = 0;
        scrollSpeedMod = .5f;
        stopRunningMod = 0;
        pressCount = 0;
        stopAnimation = true;
        stopPastBGforEnding = 1;

        //goodByeCountDown = 0.25f;
        letGo = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (letGo)
        {
            goodByeCountDown -= Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                goodByeCountDown = 0.5f;
            }

            if (goodByeCountDown <= 0)
            {

                letGo = false;
                SayGoodBye();
                Invoke("FoundTheStone", 5f);
            }

        }




        if (gameEnd == true && watchedEnding ==false)
        {
            Invoke("End", 3f);
            
        }
        else if(runningGame && watchedEnding == false)
        {
            
            Game();
        }


        /*pastCamX = presentCamX - 1;
        presentCam.rect = new Rect(presentCamX, 0, 1, 1);
        pastCam.rect = new Rect(pastCamX, 0, 1, 1);*/


    }


    void Game()
    {
        Debug.Log(pressCount);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (countToThreeToCancelInstruction >= 2)
            {
                instruction.SetActive(false);
            }
            else
            {
                countToThreeToCancelInstruction++;
            }
            gameStart = true;

            if (presentCamX <= 0.1f)
            {
                gameEnd = true;
            }
            else
            {
                pressCount++;
                presentCamX -= pressSpeed;
            }

            stopRunningMod = 1;
            stopRunningCountDown = 0.25f;
            stopAnimation = false;
        }

        if(gameEnd == false)
        {
            stopRunningCountDown -= Time.deltaTime;
        }
        

        for (int i = 0; i < pressCountSection.Length; i++)
        {
            if (i == pressCountSection.Length - 1 && pressCount > pressCountSection[i])
            {
                scrollSpeedMod = 0.5f + i * accelerate;
            }
            else if (pressCount > pressCountSection[i] && pressCount <= pressCountSection[i + 1])
            {
                scrollSpeedMod = 0.5f + i * accelerate;
            }

        }


        if (stopRunningCountDown <= 0)
        {


            if (pressCount >= 0)
            {
                pressCount -= Time.deltaTime* pressCountMinusMod;
            }
            else
            {
                stopRunningMod = 0;
                stopAnimation = true;
            }

            if (presentCamX >= 0.85f)
            {
                
            }
            else
            {
                presentCamX += Time.deltaTime * memorySpeed;
            }
        }


        pastCamX = presentCamX - 1;
        presentCam.rect = new Rect(presentCamX, 0, 1, 1);
        pastCam.rect = new Rect(pastCamX, 0, 1, 1);




    }

    
    void End()
    {
        if (presentCamX > 0)
        {
            sayGoodbye = true;
            presentCamX -= Time.deltaTime/15;
            pastCamX = presentCamX - 1;
            presentCam.rect = new Rect(presentCamX, 0, 1, 1);
            pastCam.rect = new Rect(pastCamX, 0, 1, 1);

            //fall down
            pastChar.transform.rotation = fallenRotation.rotation;
            stopPastBGforEnding = 0;

        }
        else
        {
            letGo = true;
            watchedEnding = true;
            //Invoke("SayGoodBye", 3f);

        }

    }

    void SayGoodBye()
    {
        BGMPlayer.SetActive(false);

        /*if (pastCamX >= -1)
        {
            pastCamX -= Time.deltaTime / 5;
        }*/

        KiteString.SetActive(false);
        kite.GetComponent<KiteMovement>().enabled = false;
        kite.GetComponent<Rigidbody2D>().velocity = new Vector3(-0.5f, 0.5f, 0);
        
        if (!standup)
        {

            presentChar.transform.rotation = new Quaternion(0, -90, 0, 0);
        }
        stopRunningMod = 0;
        stopAnimation = true;



    }

    void FoundTheStone()
    {
        presentCamX = 0.15f;
        pastCamX = presentCamX - 1;
        presentCam.rect = new Rect(presentCamX, 0, 1, 1);
        pastCam.rect = new Rect(pastCamX, 0, 1, 1); 


         Invoke("StandUp", 2f);

    }

    void StandUp()
    {
        standup = true;
        pastChar.transform.rotation = standUpRotation.rotation;
        FinalDialogue01.SetActive(true);
        

        Invoke("ActivateFinalDialogue02", 3f);

    }

    void ActivateFinalDialogue02()
    {
        pastChar.transform.rotation = new Quaternion(0, -90, 0, 0);
        FinalDialogue01.SetActive(false);
        FinalDialogue02.SetActive(true);

        Invoke("Black", 3f);
    }

    void Black()
    {
        presentCamX = 0;
        pastCamX = presentCamX - 1;
        presentCam.rect = new Rect(presentCamX, 0, 1, 1);
        pastCam.rect = new Rect(pastCamX, 0, 1, 1);
        Final.SetActive(true);
        Invoke("BackToMenu", 5f);
    }

    void BackToMenu()
    {
        SceneManager.LoadScene("RPG");
    }





}
