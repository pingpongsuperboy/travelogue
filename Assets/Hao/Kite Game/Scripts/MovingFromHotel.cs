using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingFromHotel : MonoBehaviour
{
    Vector3 target;

    public float walkDistance;
    public float walkSpeed;
    public static bool arrive = false;

    float percent;

    public GameObject hotel;

    int HotelMoveAway;


    public Vector3 HotelTarget;
    public Vector3 HotelTarget2;

    void Start()
    {
        HotelMoveAway = 0;
        arrive = false;
        target = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.x < 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                percent = 0;
                if (gameObject.transform.position.x > -1)
                {
                    target = new Vector3(0, transform.position.y, transform.position.z);
                    //arrived
                    arrive = true;
                    HotelMoveAway++;



                }
                else
                {
                    target = new Vector3(transform.position.x + walkDistance, transform.position.y, transform.position.z);
                }
                    
                
                

            }


        }


        percent += Time.deltaTime* walkSpeed;
        transform.position = Vector3.Lerp(transform.position, target, percent);

        if(HotelMoveAway==1)
        {
            hotel.transform.position = Vector3.Lerp(hotel.transform.position, HotelTarget, percent);
        }
        if (HotelMoveAway >= 2)
        {
            hotel.transform.position = Vector3.Lerp(hotel.transform.position, HotelTarget2, percent);
        }

    }


}
