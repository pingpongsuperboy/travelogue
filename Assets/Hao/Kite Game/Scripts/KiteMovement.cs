using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KiteMovement : MonoBehaviour
{
    public KiteGameManager myManager;
    public float percent;
    public float shakingSpeed;
    float mod = 1;

    [Header("Kite Shaking")]
    public float kiteXOffsetIinitial;
    public float kiteYOffsetIinitial;
    public float kiteXOffset;
    public float kiteYOffset;

    float newX;
    float newY;
    [Header("Kite Height")]
    public float kiteMinX;
    public float kiteMaxX;
    public float kiteMinY;
    public float kiteMaxY;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        // position

        if(myManager.gameStart == false)
        {
            newX = kiteMinX;
            newY = kiteMinY;
        }
        else
        {
            newX = map(myManager.pastCamX, -0.15f, -0.85f, kiteMinX, kiteMaxX);
            newY = map(myManager.pastCamX, -0.15f, -0.85f, kiteMinY, kiteMaxY);
        }
        
        




        //transform.position = new Vector3(newX, newY, -3);


        // shaking movement

        percent += Time.deltaTime * mod * shakingSpeed * (KiteGameManager.scrollSpeedMod);

        if (percent <= 0 || percent > 1) // make this animation loop
        {
            mod *= -1;
            percent += Time.deltaTime * mod * shakingSpeed * (KiteGameManager.scrollSpeedMod);
        }


        

        if (KiteGameManager.stopAnimation == true)
        {
            kiteXOffset = 0;
            kiteYOffset = 0;
        }
        else
        {
            kiteXOffset = kiteXOffsetIinitial * (KiteGameManager.scrollSpeedMod);
            kiteYOffset = kiteYOffsetIinitial * (KiteGameManager.scrollSpeedMod);

        }

        transform.position = Vector3.Lerp(
                new Vector3(newX + kiteXOffset, newY + kiteYOffset, -3),
                new Vector3(newX - kiteXOffset, newY - kiteYOffset, -3), percent);


    }

    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }


}
