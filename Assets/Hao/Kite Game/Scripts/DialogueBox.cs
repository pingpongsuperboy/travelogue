using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueBox : MonoBehaviour
{
    public static bool pastDialogue;
    public static bool stopAutoDialogue;
    public KiteGameManager myManager;
    public GameObject brother;
    public float cancelBrotherTextAccordingToHisPos;
    public bool isBrother;
    float time;
    public float dialogueFrequency;
    public float dialogueTime;

    public TextMeshPro dialogueText;
    int randomIndex;

    [Header("Dialogue Content(keep the first blank)")]
    public string[] dialogueSection01Content;
    public string[] dialogueSection02Content;
    public string[] dialogueSection03Content;

    public string[] dialogueSectionAloneContent;


    bool showText;
    void Start()
    {
        pastDialogue = true;
        stopAutoDialogue = false;
        showText = false;
    }

    // Update is called once per frame
    void Update()
    {
        

        if(myManager.runningGame == true && myManager.gameEnd == false)
        {
            Dialogue();
        }
        else
        {
            
            
            if (pastDialogue)
            {
                PastDialogue();
            }
            else
            {
                gameObject.GetComponent<SpriteRenderer>().color = new Vector4(1, 1, 1, 0); //transparent
                dialogueText.text = dialogueSection01Content[0];
            }
        }


    }


    void Dialogue()
    {
        if (isBrother == true &&(brother.transform.position.x < cancelBrotherTextAccordingToHisPos || brother.transform.position.x >7))
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Vector4(1, 1, 1, 0);
            dialogueText.text = dialogueSection01Content[0];

        }


        time += Time.deltaTime;

        if (time > dialogueFrequency)
        {


            //show dialogue
            if (showText == false)
            {
                if (isBrother == true && (brother.transform.position.x < cancelBrotherTextAccordingToHisPos || brother.transform.position.x > 7))
                {

                }
                else
                {
                    gameObject.GetComponent<SpriteRenderer>().color = new Vector4(1, 1, 1, 1);
                    

                    if (KiteGameManager.pressCount > 60) //section3
                    {
                        randomIndex = Random.Range(1, dialogueSection03Content.Length);
                        dialogueText.text = dialogueSection03Content[randomIndex];
                    }
                    else if (KiteGameManager.pressCount > 30)//section2
                    {
                        randomIndex = Random.Range(1, dialogueSection02Content.Length);
                        dialogueText.text = dialogueSection02Content[randomIndex];
                    }
                    else//section1
                    {
                        randomIndex = Random.Range(1, dialogueSection01Content.Length);
                        dialogueText.text = dialogueSection01Content[randomIndex];
                    }

                    
                }

                showText = true;
            }


            if (time > dialogueTime)
            {
                // cancel dialogue
                gameObject.GetComponent<SpriteRenderer>().color = new Vector4(1, 1, 1, 0);
                dialogueText.text = dialogueSection01Content[0];
                showText = false;


                time = 0;
            }

        }


    }


    void PastDialogue()
    {
        
        //--------------------------------------------

        time += Time.deltaTime;

        if (time > dialogueFrequency)
        {

            //show dialogue
            if (showText == false && stopAutoDialogue ==false)
            {
                if (isBrother == true)
                {

                }
                else
                {
                    gameObject.GetComponent<SpriteRenderer>().color = new Vector4(1, 1, 1, 1);
                    randomIndex = Random.Range(1, dialogueSectionAloneContent.Length);
                    dialogueText.text = dialogueSectionAloneContent[randomIndex];
                   
                }

                showText = true;
            }


            if (time > dialogueTime)
            {
                // cancel dialogue
                gameObject.GetComponent<SpriteRenderer>().color = new Vector4(1, 1, 1, 0);
                dialogueText.text = dialogueSection01Content[0];
                showText = false;
                time = 0;
            }

        }

        //--------------------------------------------



    }




}
