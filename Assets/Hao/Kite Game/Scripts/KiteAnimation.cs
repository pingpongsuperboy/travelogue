using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KiteAnimation : MonoBehaviour
{

    public Sprite[] sprites;
    float time;

    public float changeFrequency;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        changeFrequency = 0.75f - KiteGameManager.scrollSpeedMod * 0.1f;

        if (KiteGameManager.stopAnimation == false)
        {
            time += Time.deltaTime;

            if (time < changeFrequency)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = sprites[1];
            }else if (time >= changeFrequency && time < changeFrequency * 2)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = sprites[2];
            }else if (time >= changeFrequency*2 && time < changeFrequency *3)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = sprites[3];
            }
            else if (time >= changeFrequency * 3 && time < changeFrequency * 4)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = sprites[4];
            }
            else if (time >= changeFrequency )
            {
                time = 0;
            }



        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprites[0];

        }

        
    }
}
