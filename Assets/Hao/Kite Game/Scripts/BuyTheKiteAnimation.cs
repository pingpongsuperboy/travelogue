using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyTheKiteAnimation : MonoBehaviour
{


    float buyTheKiteTimer;
    float percent;
    float percent2;
    bool brotherSlideOut;

    bool standSlideOut;

    public float[] timing;

    public KiteGameManager myManager;

    public GameObject iWantToButAKite;
    public GameObject whichOne;
    public GameObject brother;
    public GameObject FoundYou;
    public GameObject HeyComeBack;
    public GameObject TheOneWith;
    public GameObject Kite;
    public GameObject stand;
    public Vector3 brotherTarget;
    public Vector3 standTarget;

    public GameObject instruction;
    bool instructionAppear;


    // Start is called before the first frame update
    void Start()
    {
        instructionAppear = false;
        brotherSlideOut = false;
        standSlideOut = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(myManager.runningGame == false)
        {
            buyTheKiteTimer += Time.deltaTime;

            //"I want to buy a kite"
            if (buyTheKiteTimer >= timing[0] && buyTheKiteTimer < timing[1])
            {
                
                FoundYou.SetActive(true);

                
            }


            //"which one?"

            if (buyTheKiteTimer >= timing[1] && buyTheKiteTimer < timing[2])
            {
                FoundYou.SetActive(false);
                HeyComeBack.SetActive(true);
                //ExplorationManager.PastwalkingBGspeed = .5f;
                brotherSlideOut = true;




            }


            // brother slide in 

            if (buyTheKiteTimer >= timing[2] && buyTheKiteTimer < timing[3])
            {
                
                HeyComeBack.SetActive(false);
                iWantToButAKite.SetActive(true);
                
            }

            
            if (brotherSlideOut)
            {
                ExplorationManager.BrotherPastwalkingAnimation = true;
                percent += Time.deltaTime*0.5f;
                brother.transform.position = Vector3.Lerp(brother.transform.position, brotherTarget, percent);
            }

            // BG stop + runStop + dialogue stop

            if (buyTheKiteTimer >= timing[3] && buyTheKiteTimer < timing[4])
            {
                /*
                ExplorationManager.PastwalkingBGspeed = 0;
                ExplorationManager.PastwalkingAnimation = false;*/
                iWantToButAKite.SetActive(false);
                whichOne.SetActive(true);
                
            }



            // "Found you"
            if (buyTheKiteTimer >= timing[4] && buyTheKiteTimer < timing[5])
            {
                whichOne.SetActive(false);
                TheOneWith.SetActive(true);
            }


            //Run away + "Hey come back" + BG continue
            if (buyTheKiteTimer >= timing[5] && buyTheKiteTimer < timing[6])
            {

                //DialogueBox.stopAutoDialogue = false;
                DialogueBox.pastDialogue = false;
                TheOneWith.SetActive(false);
                Kite.SetActive(true);
            }

            //dialogue start
            if (buyTheKiteTimer >= timing[6] && buyTheKiteTimer < timing[7])
            {
                //stand move
                standSlideOut = true;
                // BG
                ExplorationManager.walkingBGspeed = .5f;
                // character
                ExplorationManager.walkingAnimation = true;
            }


            // The one with yellow and blue
            if (buyTheKiteTimer >= timing[7] && buyTheKiteTimer < timing[8])
            {
                // BG
                ExplorationManager.walkingBGspeed = 0f;
                // character
                ExplorationManager.walkingAnimation = false;
                stand.SetActive(false);
            }


            // Get the kite
            if (buyTheKiteTimer >= timing[8] && buyTheKiteTimer < timing[9])
            {
                myManager.runningGame = true;

                if (instructionAppear == false)
                {
                    instruction.SetActive(true);
                    instructionAppear = true;
                }

            }

            if (standSlideOut)
            {
                percent2 += Time.deltaTime * 0.01f;
                stand.transform.position = Vector3.Lerp(stand.transform.position, standTarget, percent2);
            }


            // move forward(BG move + character +  stand move)
           /* if (buyTheKiteTimer >= timing[9] && buyTheKiteTimer < timing[10])
            {
                
            }

            

            

            if (buyTheKiteTimer >= timing[10] && buyTheKiteTimer < timing[11])
            {
                

            }

            // Game start

            if (buyTheKiteTimer >= timing[11] )
            {
                

            }*/

        }


        }
}
