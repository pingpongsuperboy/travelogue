using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KiteStringMovement : MonoBehaviour
{

    public Transform character;
    public Transform kite;

    public LineRenderer lr;

    Vector3[] positions = new Vector3[2];
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Set some positions

        positions[0] = new Vector3(character.position.x, character.position.y , character.position.z); ;
        positions[1] = new Vector3(kite.position.x+1.35f, kite.position.y-1f, kite.position.z);
        lr.SetPositions(positions);
    }
}
