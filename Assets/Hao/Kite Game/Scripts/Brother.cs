using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brother : MonoBehaviour
{
    public KiteGameManager myManager;
    public float newX;

    public float brotherMaxX;
    public float brotherMinX;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (myManager.runningGame)
        {
            if (myManager.sayGoodbye == true)
            {
                newX += Time.deltaTime / 5;
            }
            else
            {

                if (myManager.presentCamX<0.35)
                {
                    //newX = 2;
                }
                else
                {
                    newX = map(myManager.pastCamX, -0.85f, -0.15f, brotherMinX, brotherMaxX);
                }
                
            }


        }

        transform.position = new Vector3(newX, transform.position.y, transform.position.z);

    }


    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }
}
