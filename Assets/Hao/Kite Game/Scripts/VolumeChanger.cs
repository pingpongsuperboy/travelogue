using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeChanger : MonoBehaviour
{
    public KiteGameManager myManager;
    public ExplorationManager myExplorationManager;
    public bool isPast;
    public bool isBGM;
    public bool isInsect;
    AudioSource m_MyAudioSource;

    public float volume;
    
    // Start is called before the first frame update
    void Start()
    {
        m_MyAudioSource = GetComponent<AudioSource>();
        m_MyAudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        //volume = myManager.presentCamX;

        if (myManager.runningGame == false)
        {
            if (isBGM)
            {

            }
            else
            {
                if (isPast)
                {
                    if (isInsect)
                    {
                        volume = map(myExplorationManager.presentCamX, 0.5f, 1, 0, 0.75f);
                    }
                    else
                    {
                        volume = map(myExplorationManager.presentCamX, 0, 1, 0, 1);
                    }
                    
                }
                else
                {
                    volume = map(myExplorationManager.presentCamX, 0, 1, 1, 0);
                }
            }
            

        }
        else
        {
            if (isBGM)
            {
                volume = map(myManager.presentCamX, 0, 0.5f, 1, 0);
            }
            else
            {
                if (isPast)
                {
                    if (isInsect)
                    {
                        volume = map(myManager.presentCamX, 0.5f, 1, 0, 0.75f);
                    }
                    else
                    {
                        volume = map(myManager.presentCamX, 0, 1, 0, 1);
                    }
                }
                else
                {
                    volume = map(myManager.presentCamX, 0, 1, 1, 0);
                }

            }

        }

        //Debug.Log(volume);
        m_MyAudioSource.volume = volume;


        

    }
    
    float map(float value, float leftMin, float leftMax, float rightMin, float rightMax)
    {
        return rightMin + (value - leftMin) * (rightMax - rightMin) / (leftMax - leftMin);
    }

}
