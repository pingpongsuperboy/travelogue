using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorationManager : MonoBehaviour
{
    public KiteGameManager myManager;
    public GameObject stand;
    public GameObject instruction;
    public GameObject hotel;
    public GameObject brother;
    public Vector3 standTarget;
    public Vector3 brotherTarget;
    public int standMoveCount;

    float percent;
    public static bool exploration = true;
    public static bool walkingAnimation;
    public static bool PastwalkingAnimation;
    public static bool BrotherPastwalkingAnimation;
    public static float walkingBGspeed;
    public static float PastwalkingBGspeed;
    public static bool memoryArrive = false;


    bool buyTheKite;
    public GameObject buyingKiteAnimation;
    bool standMoved = false;

    bool arriveStand;

    public int timeOfMemoryComeIn;
    public int timeOfMemoryStandComeIn;

    float stopCountdown;

    int walkingPressCount;
    int walkingPressCountAfterMemory;

    int countToThreeToCancelInstruction;

    public float memoryScreenSpeed;

    public bool memoryStartToSlideIn;

    [Header("Split Screen")]
    public Camera presentCam;
    public Camera pastCam;

    public float presentCamX;
    public float pastCamX;


    void Start()
    {
        countToThreeToCancelInstruction = 0;
        arriveStand = false;
        standMoved = false;
        buyTheKite = false;
        memoryArrive = false;
        PastwalkingAnimation = true;
        PastwalkingBGspeed = 0.5f;
        presentCamX = 0;
        exploration = true;
        walkingBGspeed = .5f;
        standMoveCount = 0;
        memoryStartToSlideIn = false;
        ExplorationManager.BrotherPastwalkingAnimation = true;
    }

    void Update()
    {
        if(myManager.runningGame == false)
        {
            if (Input.GetKeyDown(KeyCode.Space) && buyTheKite == false)
            {
                if (countToThreeToCancelInstruction >= 2)
                {
                    instruction.SetActive(false);
                }
                else
                {
                    countToThreeToCancelInstruction++;
                }
                

                walkingAnimation = true;
                PastwalkingAnimation = true;
                BrotherPastwalkingAnimation = true;
                stopCountdown = 1f;
                walkingBGspeed = .5f;
                PastwalkingBGspeed = .5f;

                if (MovingFromHotel.arrive)
                {
                    walkingPressCount++;
                }

                if (memoryArrive)
                {
                    walkingPressCountAfterMemory++;
                }

                if (walkingPressCount > timeOfMemoryComeIn)
                {
                    percent = 0;
                    standMoveCount++;
                    
                }

            }

            if (stopCountdown <= 0 && buyTheKite == false)
            {
                walkingAnimation = false;
                PastwalkingAnimation = false;
                BrotherPastwalkingAnimation = false;
                walkingBGspeed = 0;
                PastwalkingBGspeed = 0;
            }
            else
            {
                stopCountdown -= Time.deltaTime;
            }

            // memory slide in
            if (walkingPressCount >= timeOfMemoryComeIn)
            {
                

                hotel.SetActive(false);


                //memory move in

                
                    if (presentCamX >= 0.85f)
                    {
                        presentCamX = 0.85f;
                        if (memoryArrive == false)
                        {
                            stand.transform.position = new Vector3(4.37f, stand.transform.position.y, stand.transform.position.z);
                        }
                        memoryArrive = true;

                    }
                    else
                    {
                        presentCamX += memoryScreenSpeed * Time.deltaTime;
                    }
                

                
            }

            // stand

            
            if(walkingPressCountAfterMemory > timeOfMemoryComeIn && standMoved == false)
            {
                // stand move in
                percent += Time.deltaTime * 0.05f;
                stand.transform.position = Vector3.Lerp(stand.transform.position, standTarget, percent);
                brother.transform.position = Vector3.Lerp(brother.transform.position, brotherTarget, percent*20);
                DialogueBox.stopAutoDialogue = true;
                buyTheKite = true;
                if(percent > 0.05f)
                {
                    walkingAnimation = false;
                    walkingBGspeed = 0;
                    PastwalkingAnimation = false;
                    BrotherPastwalkingAnimation = false;
                    PastwalkingBGspeed = 0;
                }
                buyingKiteAnimation.SetActive(true);
                if (stand.transform.position.x == standTarget.x)
                {
                    standMoved = true;
                }
            }



            //Camera (No touching)
            pastCamX = presentCamX - 1;
            presentCam.rect = new Rect(presentCamX, 0, 1, 1);
            pastCam.rect = new Rect(pastCamX, 0, 1, 1);
        }



       

    }
}
