using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRunningAnimation : MonoBehaviour
{
    public float tiredTimeGap;
    public KiteGameManager myManager;
    public Sprite[] runningSprite;

    public float timeToChangeSprite;

    public bool isPast;
    public bool isBrother;
    float time;
    float tiredTimer;


    int walkingSpriteIndex;
    int tiredSpriteIndex;
    void Start()
    {
        walkingSpriteIndex = 0;
        tiredSpriteIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        //exploartion
        if (ExplorationManager.exploration && myManager.runningGame==false)
        {

            if (isPast)
            {
                if (isBrother)
                {
                    if (ExplorationManager.BrotherPastwalkingAnimation)
                    {
                        if (time > timeToChangeSprite)
                        {
                            Walking();
                            time = 0;
                        }

                    }
                    else
                    {
                        gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[0];
                    }

                }
                else
                {

                    if (ExplorationManager.PastwalkingAnimation)
                    {
                        if (time > timeToChangeSprite)
                        {
                            Walking();
                            time = 0;
                        }

                    }
                    else
                    {
                        //Tired();
                        gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[0];
                    }
                }


                
            }
            else
            {
                if (ExplorationManager.walkingAnimation)
                {
                    if (time > timeToChangeSprite)
                    {
                        Walking();
                        time = 0;
                    }

                }
                else
                {
                    gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[0];
                }
            }
            
            

        }
        //-----------------------------------


        //running game
        if (KiteGameManager.stopAnimation == true && isBrother == false && myManager.runningGame)
        {
            if (!isPast)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[9];
            }
            else
            {
                Tired();
            }
            
        }
        else
        {
                //myManager.gameStart == true
                if (myManager.runningGame)
            {
                if (isBrother == true)
                {
                    timeToChangeSprite = 0.32f - (KiteGameManager.scrollSpeedMod * 0.045f);
                }
                else
                {
                    
                    timeToChangeSprite = 0.32f - (KiteGameManager.scrollSpeedMod * 0.05f);

                }
                if (time > timeToChangeSprite)
                {
                    if (isPast == true && isBrother == false && myManager.sayGoodbye==true)
                    {

                    }
                    else
                    {
                        time = 0;
                        Running();
                    }
                }

                    
            }
            
        }

        
        
        

    }

    void Running()
    {
        /*if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[1]  || gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[9])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[2];
        }else */
        if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[2])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[3];
        }else if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[3])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[4];
        }
        else if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[4])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[5];
        }
        else if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[5])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[6];
        }
        else if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[6])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[7];
        }
        else if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[7])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[8];
        }
        else if (gameObject.GetComponent<SpriteRenderer>().sprite == runningSprite[8])
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[1];
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[2];
        }

    }


    void Walking()
    {


        /*
         
         if (walkingSpriteIndex == 0)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[4];
                walkingSpriteIndex = 1;
            }
            else if (walkingSpriteIndex == 1)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[8];
                walkingSpriteIndex = 0;
            }*/

            if (walkingSpriteIndex == 0)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[8];
                walkingSpriteIndex = 1;
            }
            else if (walkingSpriteIndex == 1)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[1];
                walkingSpriteIndex = 2;
            }else if (walkingSpriteIndex == 2)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[2];
                walkingSpriteIndex = 3;
            }else if (walkingSpriteIndex == 3)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[3];
                walkingSpriteIndex = 4;
            }else if (walkingSpriteIndex == 4)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[4];
                walkingSpriteIndex = 5;
            }else if (walkingSpriteIndex == 5)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[5];
                walkingSpriteIndex = 6;
            }else if (walkingSpriteIndex == 6)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[6];
                walkingSpriteIndex = 7;
            }else if (walkingSpriteIndex == 7)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[7];
                walkingSpriteIndex = 0;
            }
        
        

    }


    void Tired()
    {
        //Debug.Log("tired");
        tiredTimer += Time.deltaTime;

        if (tiredTimer>=0 && tiredTimer< tiredTimeGap)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[10];
        }
        else if (tiredTimer >= tiredTimeGap && tiredTimer < tiredTimeGap*2)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[11];
        }
        else if (tiredTimer >= tiredTimeGap*2 && tiredTimer < tiredTimeGap * 3)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[12];
        }
        else if (tiredTimer >= tiredTimeGap * 3 && tiredTimer < tiredTimeGap * 4)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[13];
        }
        else if (tiredTimer >= tiredTimeGap * 4 && tiredTimer < tiredTimeGap * 5)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[14];
        }
        else if (tiredTimer >= tiredTimeGap * 5 && tiredTimer < tiredTimeGap * 6)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = runningSprite[15];
        }else if(tiredTimer >= tiredTimeGap * 6 )
        {
            tiredTimer = 0;
        }
        




    }

}
