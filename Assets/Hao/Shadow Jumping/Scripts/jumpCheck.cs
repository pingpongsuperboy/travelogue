using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpCheck : MonoBehaviour
{
    public bool isJumping;
    public GameObject leftFoot;
    public GameObject rightFoot;
    // Start is called before the first frame update
    void Start()
    {
        isJumping = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Ground")
        {
            isJumping = false;
            
        }
    }
}
