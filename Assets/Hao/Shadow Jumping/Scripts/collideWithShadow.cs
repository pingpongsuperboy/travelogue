using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideWithShadow : MonoBehaviour
{

    public GameManager myManager;
    public GameObject leftFoot;
    public GameObject rightFoot;

    public GameObject endingDialogue;
    public GameObject endingNote;
    public GameObject fadeToWhite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "people")
        {

            Debug.Log("Oops");

            myManager.scoreInt++;
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
        }

        if(other.gameObject.name == "ending")
        {

            myManager.gameStarted = false;
            leftFoot.GetComponent<Animator>().enabled = false;
            rightFoot.GetComponent<Animator>().enabled = false;
            leftFoot.transform.position = new Vector3(leftFoot.transform.position.x, 0.003038794f, leftFoot.transform.position.z);
            rightFoot.transform.position = new Vector3(rightFoot.transform.position.x, 0.003038794f, rightFoot.transform.position.z);

            Invoke("Ending01", 1f);

        }

    }

    void Ending01()
    {
        endingDialogue.SetActive(true);
        Invoke("Ending02", 2.5f);
    }

    void Ending02()
    {
        endingNote.SetActive(true);
        fadeToWhite.SetActive(true);
    }


}
