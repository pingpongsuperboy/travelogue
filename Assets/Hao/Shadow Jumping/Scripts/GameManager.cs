using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject myCamera;
    public GameObject leftFoot;
    public GameObject rightFoot;
    public GameObject scoreText;


    public GameObject paper;
    public GameObject score;

    public bool gameStarted;

    public GameObject player;
    public float speed;
    public float jumpForce;
    public Rigidbody rb;
    
    public int scoreInt;

    // Start is called before the first frame update
    void Start()
    {
        gameStarted = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(myCamera.transform.position.x>= -13 && Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Game Start");
            leftFoot.GetComponent<Animator>().enabled = true;
            rightFoot.GetComponent<Animator>().enabled = true;

            paper.SetActive(true);
            score.SetActive(true);
            gameStarted = true;
        }

        if(gameStarted == true)
        {

            // moving forward
            Vector3 newPos = player.transform.position;
            newPos.x += speed * Time.deltaTime;
            player.transform.position = newPos;

            // jump

            if (Input.GetKeyDown(KeyCode.Space) && player.GetComponent<jumpCheck>().isJumping ==false) // press space to jump, if players are in the sky, don't let them jump again or they will just fly away
            {
                player.GetComponent<jumpCheck>().isJumping = true; // cannot double jump
                rb.AddForce(new Vector3(0, jumpForce, 0));
            }

            // score

            scoreText.GetComponent<Text>().text = scoreInt.ToString();

        }


    }
}
