using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class peopleMoving : MonoBehaviour
{

    public float percent;
    float mod = 1;
    public float waitingTime;
    public Vector3 start;
    public Vector3 destination;

    bool stopped = false;


    public float xMoveDist;
    public float zMoveDist;

    // Start is called before the first frame update
    void Start()
    {
        //check this person is on the right hand side or left hand side
        if (transform.position.z > 30)
        { //left hand side

            zMoveDist = Random.Range(0f, 5f);
        }
        else // right hand side
        {
            zMoveDist = Random.Range(0f, -5f);
        }

        xMoveDist = Random.Range(-3f, 3f);

        stopped = false;
        percent = 0;
        waitingTime = Random.Range(0.5f, 2f);
        start = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        destination = new Vector3(transform.position.x+ xMoveDist, transform.position.y, transform.position.z + zMoveDist);
    }

    // Update is called once per frame
    void Update()
    {

        percent += Time.deltaTime * mod;



        if (percent <= 0 || percent > 1) // make this animation loop
        {
            if(stopped == false)
            {
                mod = 0;
                stopped = true;
                Invoke("ContinueToMove", waitingTime);
            }
            
        }

        


        transform.position = Vector3.Lerp(start, destination, percent);


    }

    private void ContinueToMove()
    {
        if (percent <= 0)
        {
            mod = 1;
        }else if (percent > 1)
        {
            mod = -1;
        }
        percent += Time.deltaTime * mod;
        stopped = false;
    }


}
