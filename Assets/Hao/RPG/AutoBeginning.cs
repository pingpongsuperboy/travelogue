using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoBeginning : MonoBehaviour
{
    public Conversation convo;
    public CameraShake cameraShakingManager;
    public bool autoStartBeginning = true;

    void Start()    
    {
        if(autoStartBeginning && !jsch.GameManager.instance.HasAtLeastOneStoneBeenFound()) {
            DialogueManager.StartConversation(convo);
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
