using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public TextMeshProUGUI speakerName, dialogue;
    public Image speakerSprite;
    public static bool dialogueIsPlaying;

    private int currentIndex;
    private Conversation currentConvo;
    private static DialogueManager instance;

    private Animator anim;
    private Coroutine typing;


    public CameraShake cameraShakingManager;

    public GameObject player;
    public playerMovement[] playerMovementScripts;

    [Header("Auto Conversation")]
    public Conversation beginningConvo02;
    public Conversation beginningConvo03;
    public Conversation Heno02;

    [Header("NPC")]
    public GameObject Heno;
    public Vector3 HenoMovingWestTarget;
    bool HenoMovingWest;
    public Vector3 HenoMovingNorthTarget;
    bool HenoMovingNorth;

    private static bool dialogStartedPlayingThisFrame;
    private static bool dialogStoppedPlayingThisFrame;
    bool spacebarRegistered;


    private void Awake()
    {
        dialogueIsPlaying = false;
        if (instance == null)
        {
            instance = this;
            anim = GetComponent<Animator>();
        }
        else
        {
            Destroy(gameObject);
        }

        dialogueIsPlaying = false;
        dialogStartedPlayingThisFrame = false;
        dialogStoppedPlayingThisFrame = false;
        spacebarRegistered = false;
    }

    public static void StartConversation(Conversation convo)
    {
        // if we just stopped playing the dialog, don't start a new convo!!
        if(dialogStoppedPlayingThisFrame)
            return;
        Debug.Log($"starting conversation!");
         
        instance.anim.SetBool("isOpen", true);
        instance.currentIndex = 0;
        instance.currentConvo = convo;
        instance.speakerName.text = "";
        instance.dialogue.text = "";
        dialogueIsPlaying =  true;
        dialogStartedPlayingThisFrame = true;
        instance.player.GetComponent<playerMovement>().moveAmount = 0f;
        foreach(var moveScript in instance.playerMovementScripts) {
            moveScript.moveAmount = 0;
        }

        instance.ReadNext();
    }

    public void ReadNext()
    {
        if(currentIndex > currentConvo.GetLength())
        {
            Debug.Log($"done with current dialog!!");
            instance.anim.SetBool("isOpen", false);
            Trigger();
            player.GetComponent<playerMovement>().moveAmount = 5.0f;
            foreach(var moveScript in instance.playerMovementScripts) {
                moveScript.moveAmount = 5;
            }
            dialogueIsPlaying = false;
            dialogStoppedPlayingThisFrame = true;
            spacebarRegistered = false;
            return;
        }
        //speakerName.text =  currentConvo.GetLineByIndex(currentIndex).speaker.GetName();

        if (typing == null)
        {
            typing = instance.StartCoroutine(TypeText(currentConvo.GetLineByIndex(currentIndex).dialogue));
        }
        else
        {
            instance.StopCoroutine(typing);
            typing = null;
            typing = instance.StartCoroutine(TypeText(currentConvo.GetLineByIndex(currentIndex).dialogue));
        }

        
        // speakerSprite.sprite = currentConvo.GetLineByIndex(currentIndex).speaker.GetSprite();
        currentIndex++;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && typing != null && !dialogStartedPlayingThisFrame) {
            spacebarRegistered = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && dialogueIsPlaying && !dialogStartedPlayingThisFrame && typing == null)
        {
            ReadNext();
        }

        if (HenoMovingWest)
        {
            float step = 5.0f * Time.deltaTime;
            
            Heno.transform.position = Vector3.MoveTowards(Heno.transform.position, HenoMovingWestTarget, step);
            if (Vector3.Distance(Heno.transform.position, HenoMovingWestTarget) < 0.01f)
            {
                HenoMovingWest = false;
            }
            
        }

        if (HenoMovingNorth)
        {
            float step = 5.0f * Time.deltaTime;

            Heno.transform.position = Vector3.MoveTowards(Heno.transform.position, HenoMovingNorthTarget, step);
            if (Vector3.Distance(Heno.transform.position, HenoMovingNorthTarget) < 0.01f)
            {
                HenoMovingNorth = false;
                Heno.SetActive(false);
            }

        }

        // if dialog started playing this frame, we're done with this frame so flip that switch off
        if(dialogueIsPlaying && dialogStartedPlayingThisFrame) {
            dialogStartedPlayingThisFrame = false;
        }
    }

    void LateUpdate()
    {
        if(!dialogueIsPlaying && dialogStoppedPlayingThisFrame) {
            dialogStoppedPlayingThisFrame = false;
        }
    }

    private IEnumerator TypeText (string text)
    {
        dialogue.text = "";
        bool complete = false;
        int index = 0;
        while (!complete)
        {
            if(spacebarRegistered) {
                dialogue.text = text;
                complete = true;
                spacebarRegistered = false;
                break;
            }

            dialogue.text += text[index];
            index++;
            yield return new WaitForSeconds(0.02f);

            if(index == text.Length )
            {
                complete = true;
            }
        }

        typing = null;
    }


    void Trigger()
    {
        if(currentConvo.name == "Beginning")
        {
            cameraShakingManager.shakeDuration = 1.5f;
            Invoke("StartNextConversation", 2f);
        }
        if (currentConvo.name == "Beginning02")
        {
            cameraShakingManager.shakeDuration = 1.5f;
            Invoke("StartNextConversation", 2f);
        }
        if (currentConvo.name == "Beginning03")
        {
            HenoMovingWest = true;
            player.GetComponent<playerMovement>().moveAmount = 5.0f;
        }
        if (currentConvo.name == "Heno")
        {
            cameraShakingManager.shakeDuration = 1.5f;
            Invoke("StartNextConversation", 2f);
        }
        if (currentConvo.name == "Heno02")
        {
            HenoMovingNorth = true;
            player.GetComponent<playerMovement>().moveAmount = 5.0f;
        }

    }

    void StartNextConversation()
    {
        if (currentConvo.name == "Beginning")
        {
            StartConversation(beginningConvo02);
        }else if (currentConvo.name == "Beginning02")
        {
            StartConversation(beginningConvo03);
        }
        else if (currentConvo.name == "Heno")
        {
            StartConversation(Heno02);
        }
    }


    
}

