using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{

    public float moveAmount = 1.0f;

    public Rigidbody2D rb;

    public Animator anim;

    public SpriteRenderer sr;

    AudioSource AS;


    public static State currentState;

    public enum State{
        up,
        down,
        left,
        right
    }

    

    // Start is called before the first frame update
    void Start()
    {
        AS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // move up
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            currentState = State.up;
            rb.velocity = new Vector2(rb.velocity.x, moveAmount);
            AS.enabled = true;

            if(moveAmount > 0)
                anim.SetBool("Moving", true);
        }
        // move down
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            currentState = State.down;
            rb.velocity = new Vector2(rb.velocity.x, -moveAmount);
            AS.enabled = true;


            if(moveAmount > 0)
                anim.SetBool("Moving", true);
            //gameObject.transform.Translate(Vector3.down * moveAmount);
        }
        // move left
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            currentState = State.left;
            rb.velocity = new Vector2(-moveAmount, rb.velocity.y);
            AS.enabled = true;


            if(moveAmount > 0)
                anim.SetBool("Moving", true);
            sr.flipX = true;
        }
        // move right
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            currentState = State.right;
            rb.velocity = new Vector2(moveAmount, rb.velocity.y);
            AS.enabled = true;


            if(moveAmount > 0)
                anim.SetBool("Moving", true);
            sr.flipX = false;
        }

        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow))
        {
            rb.velocity = Vector2.zero;

            anim.SetBool("Moving", false);
            AS.enabled = false;

        }

    }


}
