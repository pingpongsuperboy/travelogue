using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collidable : MonoBehaviour
{
    public ContactFilter2D filter;
    private BoxCollider2D boxCollider;
    private Collider2D[] hits = new Collider2D[10];

    public Conversation convo;

    public bool requireFacingUp;
    public bool requireFacingDown;
    public bool requireFacingLeft;
    public bool requireFacingRight;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //collision work
        boxCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;

            OnCollide(hits[i]);
            

            hits[i] = null;
        }
    }


    protected virtual void OnCollide(Collider2D coll)
    {
        if(coll.tag != "Player") return; // only care about colliding with player
        if (!DialogueManager.dialogueIsPlaying)
        {
            if (Input.GetKeyDown(KeyCode.Space) && requireFacingUp && playerMovement.currentState==playerMovement.State.up)
            {
                DialogueManager.StartConversation(convo);
                Debug.Log($"{gameObject.name} is talking UP");
            }
            if (Input.GetKeyDown(KeyCode.Space) && requireFacingDown && playerMovement.currentState == playerMovement.State.down)
            {
                DialogueManager.StartConversation(convo);
                Debug.Log($"{gameObject.name} is talking DOWN");
            }
            if (Input.GetKeyDown(KeyCode.Space) && requireFacingLeft && playerMovement.currentState == playerMovement.State.left)
            {
                DialogueManager.StartConversation(convo);
                Debug.Log($"{gameObject.name} is talking LEFT");
            }
            if (Input.GetKeyDown(KeyCode.Space) && requireFacingRight && playerMovement.currentState == playerMovement.State.right)
            {
                DialogueManager.StartConversation(convo);
                Debug.Log($"{gameObject.name} is talking RIGHT");
            }
        }
    }

}
