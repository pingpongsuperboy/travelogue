using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeStateAfterPickingAction : MonoBehaviour
{
    public GameObject actionOptionBox;
    public CardGameManager myManager;

    public GameObject anotherDialogue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(myManager.secondTime==true && myManager.currentTurn == 3)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                if (anotherDialogue.activeSelf == true)
                {
                    myManager.TransitionState(CardGameManager.State.Resolve);
                    actionOptionBox.SetActive(false);
                    gameObject.SetActive(false);
                }
                anotherDialogue.SetActive(true);
                
            }

            

        }
        else
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                actionOptionBox.SetActive(false);
                myManager.TransitionState(CardGameManager.State.Resolve);
                gameObject.SetActive(false);
            }
        }


        

    }
}
