using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject dialogueBox;
    public GameObject dialogue1;
    public GameObject dialogue2;
    public GameObject dialogue3;
    public GameObject dialogue4;
    public GameObject dialogue5;

    int mousePress;

    // Start is called before the first frame update
    void Start()
    {
        mousePress = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            mousePress++;
        }

        if(mousePress == 1)
        {
            dialogue1.SetActive(false);
            dialogue2.SetActive(true);

        }
        else if (mousePress == 2)
        {
            dialogue2.SetActive(false);
            dialogue3.SetActive(true);

        }
        else if (mousePress == 3)
        {
            dialogue3.SetActive(false);
            dialogue4.SetActive(true);
        }
        else if (mousePress == 4)
        {
            dialogue4.SetActive(false);
            dialogue5.SetActive(true);
        }else if (mousePress >= 5)
        {
            dialogue5.SetActive(false);
            dialogueBox.SetActive(false);
            gameManager.SetActive(true);
            Destroy(gameObject);
        }





    }

    

}
