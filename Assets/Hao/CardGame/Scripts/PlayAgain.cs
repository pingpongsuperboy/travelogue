using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayAgain : MonoBehaviour
{
    public bool isYesForFinalQuestion;
    public bool isNoForFinalQuestion;

    public GameObject yesOption;
    public GameObject noOption;

    public TextMeshPro myText;

    public GameObject FinalQuestionBox;

    public CardGameManager myManager;

    public GameObject Card;


    public GameObject waitingForInputFinal;

    public GameObject convoBetweenGames;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        
    }

    private void OnMouseDown()
    {
        if (isYesForFinalQuestion)
        {
            yesOption.SetActive(false);
            noOption.SetActive(false);
            myText.text = "Thanks, then I'll deal the cards";
            Invoke("WaitForALittle", 0.25f);
        }
        else if (isNoForFinalQuestion)
        {
            yesOption.SetActive(false);
            noOption.SetActive(false);
            myText.text = "Go away, take it, don't come back.";
            Invoke("WaitForALittle", 0.25f);
        }
        else
        {
            convoBetweenGames.SetActive(true);
        }
        
    }

    void WaitForALittle()
    {
        waitingForInputFinal.SetActive(true);

    }

}
