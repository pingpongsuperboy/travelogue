using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endingConversation : MonoBehaviour
{
    public CardGameManager myManager;
    public GameObject Card;
    public GameObject dialogBox;
    public GameObject line1;
    public GameObject line2;
    int mousePress;
    // Start is called before the first frame update
    void Start()
    {
        line1.SetActive(true);
        dialogBox.SetActive(true);
        mousePress = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            mousePress++;
        }

        if (mousePress == 2)
        {
            line1.SetActive(false);
            line2.SetActive(true);

        }else if (mousePress >= 3)
        {
            line2.SetActive(false);
            dialogBox.SetActive(false);
            myManager.DestroyAll("card");
            Card.SetActive(true);

        }
    }
}
