using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBehavior : MonoBehaviour
{
    public SpriteRenderer myRender;
    public Vector3 playPos;
    CardGameManager myManager;


    public enum State
    {
        FaceUp,
        Selected

    }

    State currentState;

    // Start is called before the first frame update
    void Start()
    {

        myManager = CardGameManager.FindInstance(); // get access to the game manager that exists in my scene

        TransitionState(State.FaceUp);
        //myRender.color = backColor;
        //Debug.Log(currentState);

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnMouseDown()
    {
        if (myManager.currentState == CardGameManager.State.Select && gameObject.transform.position.y<-2)
        {
            
            if (currentState == State.FaceUp)
            {
                TransitionState(State.Selected);
                myManager.TransitionState(CardGameManager.State.Action);
            }
        }

    }

    void TransitionState(State newState)
    {
        currentState = newState;

        switch (currentState)
        {
            case State.Selected:
                gameObject.transform.position = playPos;
                break;
            case State.FaceUp:
                //myRender.color = faceColor;
                break;
            default:
                Debug.Log("no transition for this state");
                break;

        }

    }
}
