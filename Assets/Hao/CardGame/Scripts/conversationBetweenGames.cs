using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class conversationBetweenGames : MonoBehaviour
{
    public CardGameManager myManager;
    public GameObject dialogueBox;
    public GameObject originalDialog;
    public GameObject dialogue6;
    public GameObject dialogue7;
    public GameObject dialogue8;

    int mousePress;

    public SpriteRenderer BG;
    public Sprite MiddleSmile;


    // Start is called before the first frame update
    void Start()
    {
        mousePress = 0;
        originalDialog.SetActive(false);
        dialogue6.SetActive(true);
        dialogueBox.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            mousePress++;
        }

        if (mousePress == 2)
        {
            dialogue6.SetActive(false);
            dialogue7.SetActive(true);

        }
        else if (mousePress == 3)
        {
            BG.sprite = MiddleSmile;
            dialogue7.SetActive(false);
            dialogue8.SetActive(true);

        }else if(mousePress >= 4)
        {
            dialogueBox.SetActive(false);
            dialogue8.SetActive(false);
            myManager.PlayAgain();
            Destroy(gameObject);
        }
    }
}
