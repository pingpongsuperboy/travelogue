using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardGameManager : MonoBehaviour
{
    public State currentState;
    public int cardCount;
    public GameObject cardObj;
    public float offset;

    [Header("NPC")]
    public Vector3 firstPersonCardPos;
    public Vector3 secondPersonCardPos;
    public Vector3 thirdPersonCardPos;
    public GameObject dialogueBox;
    public TextMeshPro othersAction;
    public string[] othersActionContent;
    int othersActionRandomIndex;
    public int whoIsPlaying;
    public SpriteRenderer BG;
    public Sprite neutral;
    public Sprite lookAtYou;
    public Sprite Teasing;
    public Sprite Surprise;
    public string[] goodPlayContent;
    public string[] badPlayContent;

    [Header("Action")]
    public GameObject ActionOptionBox;
    public TextMeshPro yourSayOption;
    public TextMeshPro yourActionOption;
    int yourSayRandomIndex;
    int yourActionRandomIndex;
    public string[] yourSayContent;
    public string[] yourActionContent;
    public GameObject Options;
    public TextMeshPro actionQuestionText;

    [Header("Resolve")]
    public float howManyPercentYouMayWin;
    float dice;
    bool nicePlay;
    public GameObject goodPlayDialogueBox;
    public GameObject badPlayDialogueBox;
    public GameObject getonePointDialogueBox;
    public TextMeshPro scoreText;
    public int score;
    public int howManyScoreToWin;
    public int howManyTurns;
    public int currentTurn;
    public GameObject win;
    public GameObject lose;
    public GameObject again;


    bool waitingForInput;
    public bool secondTime;
    public GameObject secondTimeOptions;
    public TextMeshPro secondTimeActionOptionText;
    public string[] secondTimeActionContent;
    public GameObject musicPlayer;

    public GameObject finalQuestion;
    public Sprite MiddleSmile;
    public Sprite Middle;

    public GameObject imSorry;
    public enum State
    {
        DrawCards,
        OthersTurn,
        Select,
        Action,
        Resolve
    }

    private static CardGameManager instance;

    //in some cases, if a variable of this game manager is NOT static, we'll need a reference to
    //the instance with the game manager
    //for example, in this case HighScore is not static
    //so, we still need an instance reference to find the game manager
    //this function will allow us to do that
    public static CardGameManager FindInstance()
    {
        return instance;
    }

    private void Awake()
    {
        //if we have already chosen a king game manager
        //(null literally means "nothing", so if instance is NOT nothing)
        //AND
        //if the king game manager is NOT this instance of the class
        //destroy this
        if (instance != null && instance != this)
        {
            //what we;re doing is ensuring that there can only
            //be one game manager in any scene
            Destroy(this);
        }
        else //otherwise, if we do not have a king game manager
        {
            //make this the king game manager
            instance = this;
            //and do not destroy this game object when we load new scenes
           // DontDestroyOnLoad(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        currentTurn = 1;
        whoIsPlaying = 0;
        TransitionState(State.DrawCards);
    }

    // Update is called once per frame
    void Update()
    {
        if (waitingForInput == true)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                OthersPlayCard();
                waitingForInput = false;

            }
        }

        if (lose.activeSelf == true)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                Again();
                lose.SetActive(false);

            }
        }

        if (badPlayDialogueBox.activeSelf)
        {
            badPlayDialogueBox.SetActive(true);
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                NextTurn();
                badPlayDialogueBox.SetActive(false);

            }
        }


    }

    public void TransitionState(State newState)
    {
        currentState = newState;
        switch (newState)
        {
            case State.DrawCards:
                if (secondTime==true)
                {
                    BG.sprite = Middle;
                }
                else
                {
                    BG.sprite = neutral;
                }
                
                DestroyAll("card");
                for (int i = 0; i < cardCount; i++)
                {
                    GameObject newCard = Instantiate(cardObj);
                    Vector3 newPos = new Vector3(
                        gameObject.transform.position.x + i * (newCard.transform.localScale.x + offset),
                        gameObject.transform.position.y,
                        0);
                    newCard.transform.position = newPos;
                }
                TransitionState(State.OthersTurn);
                break;

            case State.OthersTurn:

                OthersPlayCard();

                break;
            case State.Select:
                if (!secondTime)
                {
                    BG.sprite = lookAtYou;
                }               
                break;
            case State.Action:
                ActionOptionBox.SetActive(true);
                if (secondTime)
                {
                    secondTimeOptions.SetActive(true);
                    secondTimeActionOptionText.text = secondTimeActionContent[currentTurn - 1];
                }
                else
                {
                    Options.SetActive(true);
                }
                
                actionQuestionText.text = ("What are you gonna do?");
                yourSayRandomIndex = Random.Range(0, yourSayContent.Length);
                yourActionRandomIndex = Random.Range(0, yourActionContent.Length);
                yourSayOption.text = yourSayContent[yourSayRandomIndex];
                yourActionOption.text = yourActionContent[yourActionRandomIndex];
                break;
            case State.Resolve:
                //Random Win Condition
                dice = Random.Range(0, 100);
                if (dice < howManyPercentYouMayWin)
                {
                    //win
                    BG.sprite = Surprise;
                    goodPlayDialogueBox.GetComponent<TextMeshPro>().text = goodPlayContent[currentTurn - 1];
                    goodPlayDialogueBox.SetActive(true);
                    Invoke("GetOnePoint", 1.5f);
                }
                else
                {
                    //lose
                    if(secondTime)
                    {

                    }
                    else
                    {
                        BG.sprite = Teasing;
                    }
                    
                    if (secondTime)
                    {
                        badPlayDialogueBox.GetComponent<TextMeshPro>().text = "Jean:\n" + badPlayContent[currentTurn +2];
                    }
                    else
                    {
                        badPlayDialogueBox.GetComponent<TextMeshPro>().text = "Jean:\n" + badPlayContent[currentTurn - 1];
                    }
                    badPlayDialogueBox.SetActive(true);
                    //Invoke("NextTurn",1.5f);
                }



                break;
            default:
                Debug.Log("this state doesn't exist");
                break;

        }

    }

    void OthersPlayCard()
    {
        if (secondTime)
        {
            if(whoIsPlaying == 2)
            {
                whoIsPlaying = 4;
            }else if(whoIsPlaying == 0)
            {
                whoIsPlaying = 2;
            }
            
        }
        else
        {
            whoIsPlaying++;
        }
        

        if (whoIsPlaying == 1)
        {
            dialogueBox.SetActive(false);
            othersAction.text = (" ");
            GameObject newCard = Instantiate(cardObj);
            newCard.transform.position = firstPersonCardPos;
            Invoke("OthersDoAction", 1f);
        }else if (whoIsPlaying == 2)
        {
            dialogueBox.SetActive(false);
            othersAction.text = (" ");
            GameObject newCard = Instantiate(cardObj);
            newCard.transform.position = secondPersonCardPos;
            Invoke("OthersDoAction", 1f);
        }else if (whoIsPlaying == 3)
        {
            dialogueBox.SetActive(false);
            othersAction.text = (" ");
            GameObject newCard = Instantiate(cardObj);
            newCard.transform.position = thirdPersonCardPos;
            Invoke("OthersDoAction", 1f);
        }else if(whoIsPlaying == 4)
        {
            dialogueBox.SetActive(false);
            othersAction.text = (" ");
            whoIsPlaying = 0;
            TransitionState(State.Select);
        }
    }

    void OthersDoAction()
    {
        if (whoIsPlaying == 1)//Patch
        {
            dialogueBox.SetActive(true);
            othersActionRandomIndex = Random.Range(0, othersActionContent.Length);
            othersAction.text = ("Patch " + othersActionContent[othersActionRandomIndex]);
            waitingForInput = true;
            //Invoke("OthersPlayCard", 1.5f);
        }else if (whoIsPlaying == 2)//Jean
        {
            dialogueBox.SetActive(true);
            othersActionRandomIndex = Random.Range(0, othersActionContent.Length);
            othersAction.text = ("Jean " + othersActionContent[othersActionRandomIndex]);
            waitingForInput = true;
            // Invoke("OthersPlayCard", 1.5f);
        }
        else if (whoIsPlaying == 3)//Riley
        {
            dialogueBox.SetActive(true);
            othersActionRandomIndex = Random.Range(0, othersActionContent.Length);
            othersAction.text = ("Riley " + othersActionContent[othersActionRandomIndex]);
            waitingForInput = true;
            //Invoke("OthersPlayCard", 1.5f);
        }

        
    }


    void GetOnePoint()
    {
        score++;
        goodPlayDialogueBox.SetActive(false);
        getonePointDialogueBox.SetActive(true);
        scoreText.text = score.ToString();
        Invoke("NextTurn", 1.5f);
    }

    void NextTurn()
    {
        getonePointDialogueBox.SetActive(false);
        badPlayDialogueBox.SetActive(false);
        
        if(currentTurn >= howManyTurns)
        {
            if (score >= howManyScoreToWin)
            {
                //Win the game
                win.SetActive(true);
                BG.sprite = Surprise;
                Invoke("Again", 1.5f);
            }
            else
            {
                //lose
                if (secondTime)
                {
                    //BG.sprite = MiddleSmile;
                    imSorry.SetActive(true);
                    //lose.GetComponent<TextMeshPro>().text = "I really hate you, you know that right?";
                }
                else
                {
                    BG.sprite = Teasing;
                    lose.SetActive(true);
                }
                
                
               // Invoke("Again", 1.5f);
            }


        }
        else
        {
            currentTurn++;
            TransitionState(State.DrawCards);

        }

    }


    public void DestroyAll(string tag)
    {
        GameObject[] cards = GameObject.FindGameObjectsWithTag(tag);
        for (int i = 0; i < cards.Length; i++)
        {
            Destroy(cards[i]);
        }
    }

    void Again()
    {
        win.SetActive(false);
        lose.SetActive(false);
        if (secondTime)
        {
            finalQuestion.SetActive(true);
        }
        else
        {
            again.SetActive(true);
        }
        

    }

    public void PlayAgain()
    {
        again.SetActive(false);
        currentTurn = 1;
        whoIsPlaying = 0;
        secondTime = true;
        TransitionState(State.DrawCards);
        musicPlayer.SetActive(false);

    }


}
