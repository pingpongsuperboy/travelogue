using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickOnMagicStone : MonoBehaviour
{

    public GameObject final;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        final.SetActive(true);
        Invoke("BackToMenu", 5f);
    }

    void BackToMenu()
    {
        SceneManager.LoadScene("MagicStone");
    }


}
