using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PickAction : MonoBehaviour
{
    public CardGameManager myManager;
    public GameObject Options;
    public GameObject actionOptionBox;

    public TextMeshPro fillinText;

    string content;
    public TextMeshPro actionText;

    public GameObject waitingForInput;

    public bool isSecondTime;

    public Sprite middleOnly;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


    }

    private void OnMouseDown()
    {
        content = fillinText.text;
        //Debug.Log("picked");
        actionText.text = ("You "+ content);
        if (isSecondTime)
        {
            if (myManager.currentTurn == 3)
            {
                myManager.BG.sprite = middleOnly;
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(false);
            }
            
        }
        else
        {
            Options.SetActive(false);
        }
        
        
        Invoke("waitALittle", 0.25f);
    }

    void waitALittle()
    {
        waitingForInput.SetActive(true);
    }


}
