using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamSound : MonoBehaviour
{
    AudioSource AS;

    public AudioClip elevation;
    public AudioClip beamShooting;
    // Start is called before the first frame update
    void Start()
    {
        AS = GetComponent<AudioSource>();
        StartCoroutine(playSound());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator playSound(){
        AS.Play();
        yield return new WaitForSeconds(AS.clip.length-1.5f);
        transform.GetChild(11).gameObject.SetActive(true);
    }
}
