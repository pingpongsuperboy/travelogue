# Game Manager #

This is a brief how-to for using the Game Manager script.

## What is GameManager.cs? ###

`GameManager.cs` just manages our scene loading. To use it, you can just add to its list of levels in the scene manager (pictured below)

<img src="pictures/GameManager_inspector.PNG" alt="GameManager.cs inspector view">

For each level, there is a `SceneName` and an `Alias`. 

`SceneName` is the _literal name of your scene file_. As you can see in the screenshot, for instance, the shadow jumping game's scene file is just called "Hao," so for its scene name you have to enter "Hao." 

Because scene file names might not always be descriptive, you can add an `Alias` to the level that is hopefully more descriptive. For the shadow jumping game, the scene filename is not very descriptive: it's just "Hao." So for the alias, we entered "ShadowJumping" which helps us remember what the game actually is. The alias can be _anything you want_! It's just there to help us.

## How do I add my level to this? ###

For now, in order to avoid merge conflicts, we should just have a single person add levels. Let's just say Joey does it for now. But this is not a good longterm solution. Once a better method of adding levels has been implemented, we'll return to this documentation and add how to do this.

## How do I load levels with this? ###

For the most part, you should probably only ever call a single function: `ReturnToMenu()`. Seems unlikely we'll ever want to load new levels outside of our menu. But, in the offchance you _do_ want to load a level, here's how:

### Load via script ###

`GameManager` is a globally-available [singleton](https://en.wikipedia.org/wiki/Singleton_pattern) object. This means that you can access it easily from any script! In order to access the GameManager, you can just use the following bit of code: `jsch.GameManager.instance`. From there, you can call any of the publicly-available methods. For instance: `jsch.GameManager.instance.ReturnToMenu()`.

Here are all the available `GameManager` methods, and how to call them.
```c#
// function that returns to the menu
void ReturnToMenu()
{
    jsch.GameManager.instance.ReturnToMenu();
}

// function that loads a scene by its SceneName
void LoadLevelBySceneName()
{
    // this is the actual unity scene file name.
    string levelSceneName = "ExampleSceneName";

    // let's load a level using the scene name
    jsch.GameManager.instance.LoadSceneBySceneName(levelSceneName);
}

// function that loads a scene by its Alias
void LoadLevelByAlias()
{
    // this is the alias we gave to the level
    string levelAlias = "ExampleAlias";

    // let's load a level using the alis
    jsch.GameManager.instance.LoadSceneByAlias(levelAlias);
}
```


## Who do I talk to? ###

Joey wrote this! If you have questions, ask him :)