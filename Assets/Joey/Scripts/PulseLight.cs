using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class PulseLight : MonoBehaviour
{
    public float maxRange;
    public float minRange;
    public float pulseSpeed;

    Light light;
    bool isGrowing;


    void Start()
    {
        light = GetComponent<Light>();
        isGrowing = true;
        light.range = minRange;
    }


    void Update()
    {
        if(isGrowing) {
            Grow();
        }
        else {
            Shrink();
        }
    }


    void Grow()
    {
        light.range += pulseSpeed * Time.deltaTime;
        if(light.range >= maxRange) {
            isGrowing = false;
        }
    }


    void Shrink()
    {
        light.range -= pulseSpeed * Time.deltaTime;
        if(light.range <= minRange) {
            isGrowing = true;
        }
    }
}
