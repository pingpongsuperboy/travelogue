using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    public class ReturnToRPG : MonoBehaviour
    {
        public void Return()
        {
            jsch.GameManager.instance.ReturnToMenu();
        }
    }
}

