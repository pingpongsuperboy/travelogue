using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace jsch
{
    public class GameManager : MonoBehaviour
    {
        public enum MagicStone { 
            Train = 0, Diner = 1, Museum = 2, Movie = 3, Kite = 4, Card  = 5, WindowShrinking = 99
        }

        [System.Serializable]
        public class LevelData
        {
            [Tooltip("This is the name of your scene file")]
            public string sceneName;
            [Tooltip("This is an easier-to-remember name for the scene. You can leave this blank if you like")]
            public string alias;
            [Tooltip("This is whether you want the mouse to be locked and hidden or not.")]
            public bool hideMouse = true;
            public bool lockMouse = true;
            public MagicStone stoneReceived;

            // not exposed in inspector. only need this internally
            public bool hasBeenVisited { get; set; }
        }

        public LevelData[] levels;
        [Tooltip("This is the name of your menu scene file")]
        public string menuSceneName;
        [Tooltip("This is the name of your opening crawl scene file")]
        public string openingCrawlSceneName;

        // singleton instance
        public static GameManager instance;
        // just hardcoding this, since it shouldn't change...
        private const string MENU_SCENE_NAME = "MainMenu";
        // helper const for tracking the last scene visited
        public const string NO_SCENE_VISITED_YET = "NONE";
        // shouldn't change...
        private const string OPENING_CRAWL_SCENE_NAME = "OpeningCrawl";
        // return to menu button
        private const KeyCode RETURN_TO_MENU_KEY = KeyCode.Escape;

        private string lastSceneVisited;


        void Awake()
        {
            // ensure singleton instance 
            if(GameManager.instance != null && GameManager.instance != this) {
                Destroy(this.gameObject);
            }
            else {
                GameManager.instance = this;
            }

            lastSceneVisited = NO_SCENE_VISITED_YET;

            // mark all scenes as not visisted
            foreach(var level in levels) {
                level.hasBeenVisited = false;
            }
        }


        public void LoadSceneBySceneName(string sceneName)
        {
            if(sceneName == MENU_SCENE_NAME) {
                ReturnToMenu();
                return;
            }

            foreach(var level in levels) {
                if(level.sceneName == sceneName) {
                    LoadScene(level);
                    return;
                }
            }

            Debug.LogError($"GameManager doens't know about a scene called {sceneName}. Did you forget to add it to the levels list on the GameManager object, or did you forget to add it to the builds list?");
        }


        public void LoadSceneByAlias(string alias)
        {
            foreach(var level in levels) {
                if(level.alias == alias) {
                    LoadScene(level);
                    return;
                }
            }

            Debug.LogError($"GameManager doens't know about a scene with the alias {alias}. Did you forget to add it to the levels list on the GameManager object, or did you forget to add it to the builds list?");
        }


        public void ReturnToMenu()
        {
            ShowMouse();
            UnlockMouse();
            SceneManager.LoadScene(menuSceneName);
            
        }


        public void GoToOpeningCrawl()
        {
            HideMouse();
            LockMouse();
            SceneManager.LoadScene(openingCrawlSceneName);
            
        }

        public string GetLastSceneVisited()
        {
            return lastSceneVisited;
        }


        public bool HasStoneBeenFound(MagicStone stone)
        {
            foreach(var level in levels) {
                if(level.stoneReceived == stone)
                    return level.hasBeenVisited;
                else
                    continue;
            }

            Debug.LogError($"unable to find if unknown stone '{stone.ToString()}' has been found");
            return false;
        }


        public bool HaveAllStonesBeenFound()
        {
            // foreach level...
            foreach(var level in levels) {
                // if it hasn't been visited, then we haven't gotten all the stones
                if(!level.hasBeenVisited && level.stoneReceived != MagicStone.WindowShrinking) {
                    return false;
                }
                // else keep searching...
            }

            // all stones obtained!
            return true;
        }


        public bool HasAtLeastOneStoneBeenFound()
        {
            // foreach level...
            foreach(var level in levels) {
                // if any level has been visited, return true
                if(level.hasBeenVisited && level.stoneReceived != MagicStone.WindowShrinking) {
                    return true;
                }
                // else keep searching...
            }

            // no stones obtained!
            return false;
        }


        private void LoadScene(LevelData levelData)
        {
            lastSceneVisited = levelData.sceneName;
            levelData.hasBeenVisited = true;

            if(levelData.hideMouse) HideMouse();
            else ShowMouse();

            if(levelData.lockMouse) LockMouse();
            else UnlockMouse();

            SceneManager.LoadScene(levelData.sceneName);
        }


        void HideMouse()
        {
            Cursor.visible = false;
        }


        void LockMouse()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }


        void ShowMouse()
        {
            Cursor.visible = true;
        }


        void UnlockMouse()
        {
            Cursor.lockState = CursorLockMode.None;            
        }
    }

}
