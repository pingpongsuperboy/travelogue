using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycleMaterialTextures : MonoBehaviour
{
    public Material[] materials;
    public float rateOfChange = 1.0f;

    Renderer renderer;
    float timer;

    void Start()
    {
        renderer = GetComponent<Renderer>();
        timer = 0;
    }


    void Update()
    {
        // if enough time has passed, change the material
        if(timer >= rateOfChange) {
            var newMat = materials[Random.Range(0, materials.Length)];
            renderer.material = newMat;
            
            // and reset timer
            timer = 0;
        }

        timer += Time.deltaTime; 
    }
}
