using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    public class DialogMetadata
    {
        private string character;
        public string Character { get => character; }

        private float waitTime;
        public float WaitTime { get => waitTime; }

        private bool waitForClick;
        public bool WaitForClick { get => waitForClick; }

        private bool clearAfterWait;
        public bool ClearAfterWait { get => clearAfterWait; }

        private bool playImmediately;
        public bool PlayImmediately { get => playImmediately; }

        private string flow;
        public string Flow { get => flow; }

        private bool showOnlyOnce;
        public bool ShowOnlyOnce { get => showOnlyOnce; }

        private bool continueOnInterrupt;
        public bool ContinueOnInterrupt { get => continueOnInterrupt; }

        private bool interruptCurrentDialog;
        public bool InterruptCurrentDialog { get => interruptCurrentDialog; }

        private string triggerOnEnter;
        public string TriggerOnEnter { get => triggerOnEnter; }


        // ----------- Ink Tag Keys -------------- //
        const string CHARACTER_TAG = "char";
        const string WAIT_TAG = "wait";
        const string CLEAR_TAG = "clear";
        const string PLAY_ON_START_TAG = "playimmediately";
        const string FLOW_TAG = "flow";
        const string ONLY_ONCE_TAG = "onlyonce";
        const string CONTINUE_ON_INTERRUPT_TAG = "continueoninterrupt";
        const string INTERRUPT_TAG = "interrupt";
        const string TRIGGER_ON_TAG = "triggeron";
        const char TAG_DIVIDER = ':';


        public DialogMetadata(List<string> inkTags)
        {
            // first, set up defaults
            waitForClick = false;
            clearAfterWait = true;
            playImmediately = false;
            showOnlyOnce = false;
            continueOnInterrupt = false;
            interruptCurrentDialog = false;
            flow = "";
            triggerOnEnter = "";

            Init(inkTags);
        }

        public DialogMetadata(List<string> inkTags, DialogMetadata inheritedData)
        {
            // first, set up defaults
            waitForClick = false;
            clearAfterWait = true;
            showOnlyOnce = false;
            continueOnInterrupt = inheritedData.ContinueOnInterrupt;
            interruptCurrentDialog = inheritedData.InterruptCurrentDialog;
            playImmediately = inheritedData.PlayImmediately;
            flow = inheritedData.Flow;
            character = inheritedData.Character;
            triggerOnEnter = "";

            Init(inkTags);
        }


        void Init(List<string> inkTags)
        {
            foreach(var tag in inkTags) {
                // parse tag info. will be in format "type:data"
                string[] tagInfo = tag.Split(TAG_DIVIDER);
                string tagType = tagInfo[0].ToLower().Trim();
                string tagData;
                if(tagInfo.Length == 0) {
                    Debug.LogError($"it looks like you forgot to add a '{TAG_DIVIDER}' to the following tag: {tag}. Please go fix it!");
                    tagData = "";
                }
                else {
                    tagData = tagInfo[1].ToLower().Trim();
                }

                // handle character
                if(tagType == CHARACTER_TAG) {
                    character = tagData;
                }
                // handle wait time
                else if(tagType == WAIT_TAG) {
                    if(tagData.ToLower() == "click") {
                        waitForClick = true;
                    }
                    else {
                        waitTime = float.Parse(tagData);
                    }
                }
                // handle clear after wait
                else if(tagType == CLEAR_TAG) {
                    clearAfterWait = tagData == "true" || tagData == "yes";
                }
                // handle play on start
                else if(tagType == PLAY_ON_START_TAG) {
                    playImmediately = tagData == "true" || tagData == "yes";
                    // if we play immediately, best to default showOnlyOnce to true
                    showOnlyOnce = playImmediately;
                }
                // handle new flow
                else if(tagType == FLOW_TAG) {
                    flow = tagData;
                }
                // handle show only once
                else if(tagType == ONLY_ONCE_TAG) {
                    showOnlyOnce = tagData == "true" || tagData == "yes";
                }
                // handle continue on interrupt
                else if(tagType == CONTINUE_ON_INTERRUPT_TAG) {
                    continueOnInterrupt = tagData == "true" || tagData == "yes";
                }
                // handle interrupt
                else if(tagType == INTERRUPT_TAG) {
                    interruptCurrentDialog = tagData == "true" || tagData == "yes";
                }
                // handle trigger on entry
                else if(tagType == TRIGGER_ON_TAG) {
                    triggerOnEnter = tagData;
                }
                // handle unknown
                else {
                    Debug.LogError($"ERROR: Unknown tag encountered: {tagType}");
                }
            }
        }
    }
}

