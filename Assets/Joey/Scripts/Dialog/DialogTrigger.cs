using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    [RequireComponent(typeof(Collider))]
    public class DialogTrigger : MonoBehaviour
    {
        [Tooltip("this should match the 'triggerOn: [id]' tag in your ink file!")]
        public string id;
        public bool onlyShowOnce = true;
        public bool onlyTriggerOnPlayerEntry = true;


        void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Player" || !onlyTriggerOnPlayerEntry) {
                InkManager.instance.OnTrigger(id);

                if(onlyShowOnce) {
                    Destroy(this);
                }
            }
        }
    }

}
