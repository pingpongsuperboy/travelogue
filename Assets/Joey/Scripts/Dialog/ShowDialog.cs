using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch
{
    [RequireComponent(typeof(TMP_Text))]
    public class ShowDialog : MonoBehaviour
    {
        // ------ PUBLIC VARS -------------------------------------- //
        [Tooltip("Amount of time between typing characters. Smaller number makes them talk faster.")]
        public float timeBetweenTypingChars = 0.1f;

        // ------ PRIVATE VARS -------------------------------------- //
        TMP_Text text;
        string targetDialog;
        bool isTyping;

        Queue<Dialog> dialogQueue;


        void Awake()
        {
            text = GetComponent<TMP_Text>();
            isTyping = false;
            dialogQueue = new Queue<Dialog>();

            text.text = "";
        }


        public void QueueDialog(Dialog dialog)
        {
            dialogQueue.Enqueue(dialog);

            if(!isTyping) {
                StartCoroutine("TypeDialogText");
            }
        }


        public void InterruptDialog(List<Dialog> newDialog)
        {
            Debug.Log($"i'm interrupting~!");
            // stop what we're currently saying
            StopAllCoroutines();

            // get rid of dialog we don't continue after interruptions
            while(dialogQueue.Count > 0 && dialogQueue.Peek().metadata.ContinueOnInterrupt == false) {
                dialogQueue.Dequeue();
            }

            // store old dialog in temporary
            Queue<Dialog> dialogToContinue = new Queue<Dialog>();
            while(dialogQueue.Count > 0) {
                dialogToContinue.Enqueue(dialogQueue.Dequeue());
            }

            // add new dialog
            foreach(var d in newDialog) {
                dialogQueue.Enqueue(d);
            }
            // add dialog to continue
            while(dialogToContinue.Count > 0) {
                dialogQueue.Enqueue(dialogToContinue.Dequeue());
            }

            // continue talking
            StartCoroutine("TypeDialogText");
        }


        IEnumerator TypeDialogText()
        {
            isTyping = true;

            // as long as we have more dialog to show...
            while(dialogQueue.Count > 0) {
                Dialog dialog = dialogQueue.Dequeue();
                text.text = dialog.text;
                text.maxVisibleCharacters = 0;

                // show character one by one, pausing between characters
                while(text.maxVisibleCharacters < text.text.Length) {
                    text.maxVisibleCharacters++;

                    yield return new WaitForSeconds(timeBetweenTypingChars);
                }

                // wait time for click
                if(dialog.metadata.WaitForClick) {
                    while(!Input.GetMouseButtonDown(0)) {
                        yield return null;
                    }
                }
                // or wait time specified
                else {
                    yield return new WaitForSeconds(dialog.metadata.WaitTime);
                }

                // wipe text if asked for
                if(dialog.metadata.ClearAfterWait) {
                    text.text = "";
                }
            }
            isTyping = false;
        }
    }
}

