using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using TMPro;
using System.Text.RegularExpressions;
using System.Linq;

namespace jsch
{
    public class InkManager : MonoBehaviour
    {
        // ---------- Helper classes ----------------------- //
        [System.Serializable]
        public class Character {
            public string characterName;
            public ShowDialog showDialog;
            public AIController aiController;
        }

        protected class StoryBranch {
            public string flow;
            public int choiceIndex;
            public string id;
        }
        // id to signal we should play this when we see it
        const string PLAY_BRANCH_ON_START_ID = "PLAY";

        // ---------- Public vars -------------------------- //
        [Tooltip("Put the .json file in here, not the .ink file")]
        public TextAsset inkAsset;

        [Tooltip("For each character in the scene with dialog, add their name and a reference to the Text Mesh Pro object to write to")]
        public Character[] charactersInScene; 

        // singleton instance
        public static InkManager instance;



        // ---------- Private vars -------------------------- //
        // the ink story we're wrapping
        Story inkStory;

        // list of branches currently available to us
        List<StoryBranch> activeBranches;
        // list of dialog options available
        List<Dialog> activeDialogOptions;

        // regex for parsing knot navigation
        const string GOTO_REGEX = "(?<=GOTO\\().+?(?=\\))";
        // regex for parsing destination
        const string DESTINATION_REGEX = "(?<=SET_DESTINATION\\().+?(?=\\))";




        // ============================================================================= // 
        // ==========    Lifetime Methods         ====================================== //
        // ============================================================================= //

        void Awake()
        {
            // ensure singleton instance 
            if(InkManager.instance != null && InkManager.instance != this) {
                Destroy(this.gameObject);
            }
            else {
                InkManager.instance = this;
            }

            // init ink story from josn file
            inkStory = new Story(inkAsset.text);
            // init branches
            activeBranches = new List<StoryBranch>();
            activeDialogOptions = new List<Dialog>();

            // set character names to lower to avoid casing mismatches
            foreach(var c in charactersInScene) {
                c.characterName = c.characterName.ToLower();
            }
        }


        void Start()
        {
            LoadCurrentFlow();
        }




        // ============================================================================= // 
        // ==========    Main Methods         ========================================== //
        // ============================================================================= //

        void ShowDialog(Dialog dialog)
        {
            // check if this is a navigation declaration
            if(IsNavigation(dialog.text)) {
                // if so, parse knot requested...
                var regex = new Regex(GOTO_REGEX);
                string targetKnot = regex.Matches(dialog.text)[0].Value;

                // and go there
                inkStory.ChoosePathString(targetKnot);
                LoadCurrentFlow();
            }
            // check if we're setting a destination
            else if(IsSettingDestination(dialog.text)) {
                // if so, parse destination
                var regex = new Regex(DESTINATION_REGEX);
                string targetDestination = regex.Matches(dialog.text)[0].Value;

                // and tell character to go there
                Character characterTalking = GetCharacter(dialog.metadata.Character);
                characterTalking.aiController.SetDestination(targetDestination);    
            }
            else {  
                // get the character we want
                Character characterTalking = GetCharacter(dialog.metadata.Character);

                // set their text accordingly
                characterTalking.showDialog.QueueDialog(dialog);
            }

            // if show only once, remove from list
            if(dialog.metadata.ShowOnlyOnce) {
                var currentDialog = activeDialogOptions.Single(d => d.text == dialog.text);
                activeDialogOptions.Remove(currentDialog);
            }            
        }


        void LoadStoryBranches()
        {
            string currentFlow = inkStory.currentFlowName;
            for(int i = 0; i < inkStory.currentChoices.Count; i++) {
                // build branch from choice
                StoryBranch branch = new StoryBranch();
                branch.id = inkStory.currentChoices[i].text.Trim();
                branch.choiceIndex = i;
                branch.flow = inkStory.currentFlowName;

                // add to global list
                activeBranches.Add(branch);
            }
        }


        void LoadCurrentFlow()
        {
            DialogMetadata knotMetadata = null;
            while(inkStory.canContinue) {
                var dialog = GetNextDialog(ref knotMetadata);

                // add to available dialog
                activeDialogOptions.Add(dialog);

                // if interruption, interrupt the dialog
                if(dialog.metadata.InterruptCurrentDialog) {
                    Debug.Log("got interruption!");
                    InterruptDialog(dialog, ref knotMetadata);
                }
                else {
                    // if requested, show immediately
                    if(dialog.metadata.PlayImmediately) {
                        ShowDialog(dialog);
                    }   
                }                             
            }
        }


        public void OnTrigger(string id)
        {
            // look to see if we have any active dialog triggered with this id
            Dialog matchingDialog = GetDialogOnTrigger(id);

            Debug.Log($"trying to trigger dialog with id: {id}");

            // if not, just return
            if(matchingDialog == null) {
                Debug.Log($"no dialog with trigger {id} found");
                return;
            }
            // otherwise, go ahead and show it!
            else {
                // check if this is a navigation declaration
                if(IsNavigation(matchingDialog.text)) {
                    // if so, parse knot requested...
                    var regex = new Regex(GOTO_REGEX);
                    string targetKnot = regex.Matches(matchingDialog.text)[0].Value;

                    // and go there
                    inkStory.ChoosePathString(targetKnot);
                    LoadCurrentFlow();
                }
            }
        }




        // ============================================================================= // 
        // ==========    Helper Methods         ======================================== //
        // ============================================================================= //

        Character GetCharacter(string characterName)
        {
            foreach(var character in charactersInScene) {
                if(character.characterName == characterName) {
                    return character;
                }
            }

            Debug.LogError($"Unknown character in ink file {inkAsset.name}: {characterName}");
            return new Character();
        }


        bool IsNavigation(string text)
        {
            return text.StartsWith("GOTO(");
        }

        bool IsSettingDestination(string text)
        {
            return text.StartsWith("SET_DESTINATION(");
        }


        void LoadBranch()
        {
            List<Dialog> branchDialog = new List<Dialog>();
            while(inkStory.canContinue) {
                // init new dialog
                Dialog dialog = new Dialog();

                // get text and metadata
                dialog.text = inkStory.Continue();
                dialog.metadata = new DialogMetadata(inkStory.currentTags);

                // add to list
                branchDialog.Add(dialog);
            }

            foreach(var dialog in branchDialog) {
                ShowDialog(dialog);
            }
        }


        void InterruptDialog(Dialog initialInterruption, ref DialogMetadata knotMetadata)
        {
            List<Dialog> interruption = new List<Dialog>();
            interruption.Add(initialInterruption);

            while(inkStory.canContinue) {
                var dialog = GetNextDialog(ref knotMetadata);

                // add to available dialog
                activeDialogOptions.Add(dialog);

                // if no longer interrupting, stop
                if(!dialog.metadata.InterruptCurrentDialog) {
                    break;
                }
            }

            // queue the full interruption
            Character character = GetCharacter(initialInterruption.metadata.Character);
            character.showDialog.InterruptDialog(interruption);
        }


        Dialog GetNextDialog(ref DialogMetadata knotMetadata)
        {
            // init new dialog
            Dialog dialog = new Dialog();

            // get the next dialog
            dialog.text = inkStory.Continue();

            // get the current tags
            if(knotMetadata != null) {
                dialog.metadata = new DialogMetadata(inkStory.currentTags, knotMetadata);
            }
            else {
                knotMetadata = new DialogMetadata(inkStory.currentTags);
                dialog.metadata = knotMetadata;
            }

            return dialog;
        }


        Dialog GetDialogOnTrigger(string id)
        {
            foreach(var d in activeDialogOptions) {
                if(d.metadata.TriggerOnEnter == id) {
                    return d;
                }
            }

            return null;
        }
    }
}

