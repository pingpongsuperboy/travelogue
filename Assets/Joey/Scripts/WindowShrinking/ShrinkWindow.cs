using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkWindow : MonoBehaviour
{
    [System.Serializable]
    public class Resolution
    {
        public int width;
        public int height;
    }

    public float shrinkingTime = 10.0f;
    public bool shrinkOnAwake = false;
    public Resolution targetResolution;
    public int refreshRate = 60;
    

    bool isShrinking;
    float timer;
    Resolution startingResolution;

    void Start()
    {
        isShrinking = shrinkOnAwake;
        timer = 0;

        startingResolution = new Resolution();
        startingResolution.width = Screen.resolutions[Screen.resolutions.Length - 1].width;
        startingResolution.height = Screen.resolutions[Screen.resolutions.Length - 1].height;
    }


    void Update()
    {
        if(!isShrinking) return; // wait to start shrinking

        timer += Time.deltaTime;
        SetScreenSize();

        if(timer >= shrinkingTime) {
            Application.Quit();
        }
    }


    public void StartShrinking()
    {
        isShrinking = true;
    }


    void SetScreenSize()
    {
        int width = Mathf.RoundToInt(Mathf.Lerp(startingResolution.width, targetResolution.width, timer / shrinkingTime));
        int height = Mathf.RoundToInt(Mathf.Lerp(startingResolution.height, targetResolution.height, timer / shrinkingTime));

        Debug.Log($"setting screen size to {width} x {height}");

        Screen.SetResolution(width, height, FullScreenMode.Windowed, refreshRate);
    }
}
