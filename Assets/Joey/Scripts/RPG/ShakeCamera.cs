using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch.RPG
{
    public class ShakeCamera : MonoBehaviour
    {
        public float duration = 1.0f;
        public CameraShake cameraShake;


        public void Shake()
        {
            cameraShake.shakeDuration = duration;
        }
    }
}

