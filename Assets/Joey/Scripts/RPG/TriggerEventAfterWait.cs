using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventAfterWait : MonoBehaviour
{
    public UnityEvent eventToTrigger;
    public float waitTime;


    public void Trigger()
    {
        Invoke("DoEvent", waitTime);
    }


    void DoEvent()
    {
        eventToTrigger.Invoke();
    }
}
