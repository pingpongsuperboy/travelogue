using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HologramSages : MonoBehaviour
{
    public Color darkestColor;
    public Color brightestColor;
    public float fadeInTime;
    public float pulseTime;
    public AnimationCurve fadeInCurve;
    public bool fadeOnAwake = false;


    bool fadingIn;
    bool isPulsing;
    bool pulsingUp;
    float timer;
    SpriteRenderer sprite;

    void Start()
    {
        fadingIn = fadeOnAwake;
        isPulsing = fadingIn;
        timer = 0;
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = Color.black;
        pulsingUp = false;
    }


    void Update()
    {
        // wait to pulse
        if(!isPulsing) return;


        if(fadingIn) 
            FadeIn();
        else
            Pulse();
    }


    void FadeIn()
    {
        if(timer > fadeInTime) {
            fadingIn = false;
        }

        // change timer to value [0, 1]
        float lerpValue = Mathf.Lerp(0, brightestColor.r, timer / fadeInTime);
        lerpValue = fadeInCurve.Evaluate(lerpValue);


        Color spriteColor = new Color(lerpValue, lerpValue, lerpValue);
        sprite.color = spriteColor;

        timer += Time.deltaTime;
    }


    void Pulse()
    {
        float pulseValue = (pulsingUp? pulseTime : -pulseTime) * Time.deltaTime;
        Color newColor = new Color(sprite.color.r + pulseValue, sprite.color.r + pulseValue, sprite.color.r + pulseValue);
        sprite.color = newColor;

        if(pulsingUp && sprite.color.r > brightestColor.r) {
            pulsingUp = false;
        }
        else if(!pulsingUp && sprite.color.r < darkestColor.r) {
            pulsingUp = true;
        }
    }


    public void BeginFadeIn()
    {
        Debug.Log("beginning fade in!!!!");
        fadingIn = true;
        isPulsing = true;
    }
}
