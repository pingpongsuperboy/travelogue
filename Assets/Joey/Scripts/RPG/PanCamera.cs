using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanCamera : MonoBehaviour
{
    public float moveSpeed;
    public bool moveOnAwake;


    bool isMoving;

    void Start()
    {
        isMoving = moveOnAwake;
    }


    void Update()
    {
        if(!isMoving) return; // wait to move

        transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
    }


    public void StartMoving()
    {
        isMoving = true;
    }
}
