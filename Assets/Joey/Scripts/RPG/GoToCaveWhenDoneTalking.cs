using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace jsch.RPG
{
    public class GoToCaveWhenDoneTalking : MonoBehaviour
    {
        public UnityEvent eventsToGoToCave;
        public float pauseBeforeGoing = 1.0f;

        public ContactFilter2D filter;
        private BoxCollider2D boxCollider;
        private Collider2D[] hits = new Collider2D[10];


        bool mayeIsTalking;
        bool goingToCave;
        bool wasDialogPlayingLastFrame;


        // Start is called before the first frame update
        protected virtual void Start()
        {
            boxCollider = GetComponent<BoxCollider2D>();
            mayeIsTalking = false;
            goingToCave = false;
            wasDialogPlayingLastFrame = false;
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            // not talking to maye yet
            // listen for when that starts
            if(!mayeIsTalking) {
                //collision work
                boxCollider.OverlapCollider(filter, hits);
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i] == null)
                        continue;

                    OnCollide(hits[i]);
                    

                    hits[i] = null;
                }
            }
            // is talking to maye
            // wait for conversation to end
            else {
                if(DialogueManager.dialogueIsPlaying == false && !goingToCave) {
                    Debug.Log("[GotoCave] done talking!");
                    goingToCave = true;
                    gameObject.GetComponent<Collidable>().enabled = false;
                    Invoke("GoToCave", pauseBeforeGoing);
                }
            }
        }


        protected virtual void OnCollide(Collider2D coll)
        {
            if(coll.tag != "Player") return;
            Debug.Log($"{gameObject.name} registered collision!");

            // if dialog WASN'T playing last frame, but it is now, our conversation has started!
            if(!wasDialogPlayingLastFrame && DialogueManager.dialogueIsPlaying) {
                mayeIsTalking = true;
            }

            wasDialogPlayingLastFrame = DialogueManager.dialogueIsPlaying;



            // if (!DialogueManager.dialogueIsPlaying)
            // {
            //     Debug.Log($"{gameObject.name} registered collision AND dialog is not playing!");
            //     if (Input.GetKeyDown(KeyCode.Space))
            //     {
            //         Debug.Log($"{gameObject.name} is talking now! getting ready to warp...");
            //         mayeIsTalking = true;
            //     }
            // }
        }


        void GoToCave()
        {
            Debug.Log($"{gameObject.name} GOING TO CAVE");
            eventsToGoToCave.Invoke();
            this.enabled = false;
        }
    }
}

