using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace jsch
{
    public class EventWhenDialogFinished : MonoBehaviour
    {
        public UnityEvent eventOnReady;
        public float pauseBeforeEventInvocation = 1;
        public bool listenForDialogOnAwake = false;


        bool waitingForDialogToFinish;
        bool triggeringEvent;


        void Start()
        {
            waitingForDialogToFinish = listenForDialogOnAwake;
            triggeringEvent = false;
        }


        void Update()
        {
            if(!waitingForDialogToFinish) return; // not waiting for dialog yet, just do nothing

            if(DialogueManager.dialogueIsPlaying == false && !triggeringEvent) {
                    triggeringEvent = true;
                    Invoke("TriggerEvents", pauseBeforeEventInvocation);
                }
        }


        public void StartListeningForDialogToFinish()
        {
            waitingForDialogToFinish = true;
        }


        void TriggerEvents()
        {
            eventOnReady.Invoke();
            this.enabled = false;
        }
    }
}
