using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowDialogOnAwake : MonoBehaviour
{
    public Conversation conversation;
    public float waitTime;
    public bool requireNoStones = false;


    void Start()
    {
        // if we only want to show if no stones have been found, and we HAVE found a stone, return
        if(requireNoStones && jsch.GameManager.instance.HasAtLeastOneStoneBeenFound()) return;

        Invoke("ShowDialog", waitTime);
    }



    void ShowDialog()
    {
        DialogueManager.StartConversation(conversation);
    }
}
