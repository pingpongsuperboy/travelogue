using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch.RPG
{
    public class LoadMagicStoneScene : MonoBehaviour
    {
        void Update()
        {
            // 1 == train
            if(Input.GetKeyDown(KeyCode.Alpha1)) {
                jsch.GameManager.instance.LoadSceneByAlias("train");
            }
            // 2 == diner
            if(Input.GetKeyDown(KeyCode.Alpha2)) {
                jsch.GameManager.instance.LoadSceneByAlias("diner");
            }
            // 3 == museum
            if(Input.GetKeyDown(KeyCode.Alpha3)) {
                jsch.GameManager.instance.LoadSceneByAlias("museum");
            }
            // 4 == movie
            if(Input.GetKeyDown(KeyCode.Alpha4)) {
                jsch.GameManager.instance.LoadSceneByAlias("movie");
            }
            // 5 == kite
            if(Input.GetKeyDown(KeyCode.Alpha5)) {
                jsch.GameManager.instance.LoadSceneByAlias("kite");
            }
            // 6 == card
            if(Input.GetKeyDown(KeyCode.Alpha6)) {
                jsch.GameManager.instance.LoadSceneByAlias("card");
            }
        }
    }

}
