using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RPGFadeToWhite : MonoBehaviour
{
    public float startingAlpha;
    public float endingAlpha;
    public float fadeTime;
    public bool fadeOnAwake;
    public UnityEvent eventOnFinish;


    Image image;
    float timer;
    Color imageColor;
    bool isFading;

    void Start()
    {
        image = GetComponent<Image>();
        timer = 0;
        imageColor = Color.white;
        imageColor.a = startingAlpha;
        image.color = imageColor;
        isFading = fadeOnAwake;
    }


    void Update()
    {
        if(!isFading) return; // keep waiting

        Fade();

        if(timer < fadeTime) return; // still fading, bail out

        image.color = new Color(1, 1, 1, endingAlpha);
        eventOnFinish.Invoke();
        this.enabled = false;
    }


    public void BeginFade()
    {
        isFading = true;
    }


    void Fade()
    {
        float alpha = Mathf.Lerp(startingAlpha, endingAlpha, timer / fadeTime);
        imageColor.a = alpha;
        image.color = imageColor;

        timer += Time.deltaTime;
    }
}
