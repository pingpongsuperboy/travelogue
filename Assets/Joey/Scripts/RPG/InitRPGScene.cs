using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace jsch.RPG
{
    public class InitRPGScene : MonoBehaviour
    {
        [System.Serializable]
        public class MagicStoneEvent
        {
            public jsch.GameManager.MagicStone magicStone;
            public UnityEvent eventIfFound;
        }

        [Tooltip("If the player has obtained the corresponding magic stone, we'll invoke the event on Start")]
        public MagicStoneEvent[] magicStoneEvents;
        public UnityEvent eventOnAnyStonesFound;
        public UnityEvent eventOnAllStonesFound;


        void Start()
        {
            if(jsch.GameManager.instance.HasAtLeastOneStoneBeenFound())
                eventOnAnyStonesFound.Invoke();

            foreach(var magicStoneEvent in magicStoneEvents) {
                if(jsch.GameManager.instance.HasStoneBeenFound(magicStoneEvent.magicStone)) {
                    magicStoneEvent.eventIfFound.Invoke();
                }
            }

            if(jsch.GameManager.instance.HaveAllStonesBeenFound())
                eventOnAllStonesFound.Invoke();
        }


        public void EndGame()
        {
            jsch.GameManager.instance.LoadSceneByAlias("windowShrinking");
        }
    }
}

