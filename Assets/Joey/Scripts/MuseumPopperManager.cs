using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuseumPopperManager : MonoBehaviour
{
    public MuseumPopperSound[] poppers;
    public float minTime;
    public float maxTime;
    public float fadeTime;
    public float maxVolume;
    public AnimationCurve fadeCurve;


    public void StartPopping()
    {
        Debug.Log("popping!");
        foreach(var popper in poppers) {
            // var popper = new GameObject(clip.name);
            // popper.transform.parent = transform;
            // popper.AddComponent<MuseumPopperSound>();
            // popper.GetComponent<MuseumPopperSound>().AssignClip(clip, Random.Range(minTime, maxTime), fadeTime, maxVolume, fadeCurve);
            popper.StartSomePops();
        }
    }
}
