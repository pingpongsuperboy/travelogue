using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class CycleSprites : MonoBehaviour
    {
        [System.Serializable]
        public class SpriteData
        {
            public Sprite sprite;
            public float minTime = 0.1f;
            public float maxTime = 1.0f;
            public bool changeTransform = false;
            public Vector3 newPosition;
            public Vector3 newScale;
        }


        public SpriteData[] spritesToCycle;
        public float waitTimeToStart = 0.0f;

        SpriteRenderer renderer;
        float timer;
        float targetTime;
        int index;

        void Start()
        {
            renderer = GetComponent<SpriteRenderer>();
            index = 0; 
            timer = 0;
            
            targetTime = waitTimeToStart + 1.0f;
            Invoke("BeginCycling", waitTimeToStart);
        }


        void Update()
        {
            if(timer >= targetTime)
            {
                CycleSprite();
                timer = 0;
                SetTargetTime();
            }

            timer += Time.deltaTime;
        }


        void CycleSprite()
        {
            index = (index + 1) % spritesToCycle.Length;
            var spriteData = spritesToCycle[index];
            renderer.sprite = spriteData.sprite;

            if(spriteData.changeTransform) {
                transform.position = spriteData.newPosition;
                transform.localScale = spriteData.newScale;
            }
        }


        void BeginCycling()
        {
            timer = 0;
            SetTargetTime();
        }


        void SetTargetTime()
        {
            targetTime = Random.Range(spritesToCycle[index].minTime, spritesToCycle[index].maxTime);
        }
    }
}

