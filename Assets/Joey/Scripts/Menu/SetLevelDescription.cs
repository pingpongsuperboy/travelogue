using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Menu 
{
    [RequireComponent(typeof(TMP_Text))]
    public class SetLevelDescription : MonoBehaviour
    {
        private string _header;
        public string header { get { return _header; } }

        private string _description;
        public string description { get { return _description; } }

        private TMP_Text textMesh;


        void Awake()
        {
            textMesh = GetComponent<TMP_Text>();
            UpdateTextMesh();
        }


        public void SetHeader(string newHeader)
        {
            _header = newHeader;
            UpdateTextMesh();
        }

        public void SetDescription(string newDescription)
        {
            _description = newDescription;
            UpdateTextMesh();
        }

        public void Clear()
        {
            _header = "";
            _description = "";
            UpdateTextMesh();
        }


        void UpdateTextMesh()
        {
            textMesh.text = $"{_header}\n\n{_description}";
        }
    }
}

