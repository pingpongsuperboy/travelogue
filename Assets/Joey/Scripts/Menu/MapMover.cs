using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Menu.Map
{
    public class MapMover : MonoBehaviour
    {
        [System.Serializable]
        public class LevelInfo
        {
            public string description;
            public Vector2 coordinates;
            public Vector3 cameraCoordinates;
            public Vector2 startingPlayerCoordinates;
            public string sceneNameToLoad;
            public GameObject levelDescriptionObject;
            public TMP_Text descriptionTextMesh;
        }


        public LevelInfo[] levels;
        public Transform player;
        public float moveAmount = 1.0f;

        int currentDestinationIndex;
        LevelInfo currentDestination;


        void Start()
        {
            Debug.Log("starting scene!");
            SetDestination();
            HideLevelDescription();
            SetPlayerAndCameraPositions();
        }


        void Update()
        {
            MovePlayer();

            // wait for player to reach destination
            if(!HasReachedDestination()) {
                // make sure level description is hidden
                HideLevelDescription();
                return; 
            }

            ShowLevelDescription();
        }


        void MovePlayer()
        {
            // move up
            if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
                player.Translate(Vector3.up * moveAmount);
            }
            // move down
            if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.LeftArrow)) {
                player.Translate(Vector3.down * moveAmount);
            }
            // move left
            if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.DownArrow)) {
                player.Translate(Vector3.left * moveAmount);
            }
            // move right
            if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
                player.Translate(Vector3.right * moveAmount);
            }
        }


        bool HasReachedDestination()
        {
            Vector2 playerPositionV2 = new Vector2(player.position.x, player.position.y);
            return Vector2.Distance(playerPositionV2, currentDestination.coordinates) < 0.3f;
        }


        void ShowLevelDescription()
        {
            if(!currentDestination.levelDescriptionObject.activeInHierarchy) {
                currentDestination.levelDescriptionObject.SetActive(true);

                // set description text
                currentDestination.descriptionTextMesh.text = currentDestination.description;
            }
        }


        void HideLevelDescription()
        {
            currentDestination.levelDescriptionObject.SetActive(false);

            foreach(var level in levels) {
                if(level.levelDescriptionObject == null) continue;
                level.levelDescriptionObject.SetActive(false);
            }
        }


        void SetDestination()
        {
            // query Game Manager for last scene visited
            string lastSceneVisited = jsch.GameManager.instance.GetLastSceneVisited();
            Debug.Log($"last scene visited: {lastSceneVisited}");

            // if we haven't seen any scenes, just set destination to 0
            if(lastSceneVisited == jsch.GameManager.NO_SCENE_VISITED_YET) {
                currentDestinationIndex = 0;
                currentDestination = levels[0];
                return;
            }

            // otherwise, get the index of the last scene visited
            int levelIndex = 0;
            for(levelIndex = 0; levelIndex < levels.Length; levelIndex++) {
                if(levels[levelIndex].sceneNameToLoad == lastSceneVisited)
                    break;
            }

            // if we didn't find a match, set destination to 0 and throw an error
            if(levelIndex >= levels.Length) {
                Debug.LogError($"last scene visited was {lastSceneVisited} but no match was found in levels");
                currentDestinationIndex = 0;
                currentDestination = levels[0];
                return;
            }

            // otherwise, set current destination to last level + 1
            currentDestinationIndex = levelIndex + 1;
            currentDestination = levels[currentDestinationIndex];

            // and set the camera to where we're at
            Camera.main.transform.position = levels[levelIndex].cameraCoordinates;
            player.position = levels[levelIndex].startingPlayerCoordinates;

            Debug.Log($"destination: {currentDestination.sceneNameToLoad}");
        }


        async void SetPlayerAndCameraPositions()
        {
            player.transform.position = new Vector3(currentDestination.startingPlayerCoordinates.x, currentDestination.startingPlayerCoordinates.y, player.transform.position.z);
            Camera.main.transform.position = currentDestination.cameraCoordinates;
        }
    }
}

