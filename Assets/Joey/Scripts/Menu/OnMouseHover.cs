using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnMouseHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public UnityEvent eventOnHover;
    public UnityEvent eventOnExit;

    public void OnPointerEnter(PointerEventData eventData)
    {
        eventOnHover.Invoke();
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        eventOnExit.Invoke();
    }
}
