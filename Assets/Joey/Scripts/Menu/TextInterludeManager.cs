using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Menu.TextInterlude
{
    public class TextInterludeManager : MonoBehaviour
    {
        [System.Serializable]
        public class Interlude
        {
            public string sceneNameToLoad;
            public GameObject textMesh;
            public float waitTimeBeforeLoadingNextScene;
        }

        public Interlude[] interludes;

        Interlude currentInterlude;

        void Start()
        {
            HideText();
            SetText();
        }


        void SetText()
        {
            // query Game Manager for last scene visited
            string lastSceneVisited = jsch.GameManager.instance.GetLastSceneVisited();
            Debug.Log($"last scene visited: {lastSceneVisited}");

            // if we haven't seen any scenes, just set destination to 0
            if(lastSceneVisited == jsch.GameManager.NO_SCENE_VISITED_YET) {
                currentInterlude = interludes[0];
                currentInterlude.textMesh.SetActive(true);
                Invoke("LoadNextScene", currentInterlude.waitTimeBeforeLoadingNextScene);
                return;
            }

            // otherwise, get the index of the last scene visited
            int levelIndex = 0;
            for(levelIndex = 0; levelIndex < interludes.Length; levelIndex++) {
                if(interludes[levelIndex].sceneNameToLoad == lastSceneVisited)
                    break;
            }

            // if we didn't find a match, set destination to 0 and throw an error
            if(levelIndex >= interludes.Length) {
                Debug.LogError($"last scene visited was {lastSceneVisited} but no match was found in levels");
                currentInterlude = interludes[0];
                currentInterlude.textMesh.SetActive(true);
                Invoke("LoadNextScene", currentInterlude.waitTimeBeforeLoadingNextScene);
                return;
            }

            // otherwise, set current destination to last level + 1
            currentInterlude = interludes[levelIndex + 1];
            currentInterlude.textMesh.SetActive(true);
            Invoke("LoadNextScene", currentInterlude.waitTimeBeforeLoadingNextScene);
        }


        void LoadNextScene()
        {
            Debug.Log("loading scene!");
            jsch.GameManager.instance.LoadSceneBySceneName(currentInterlude.sceneNameToLoad);
        }


        void HideText()
        {
            foreach(var interlude in interludes) {
                interlude.textMesh.SetActive(false);
            }
        }
    }
}

