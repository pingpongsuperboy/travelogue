using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuseumPopperSound : MonoBehaviour
{
    public float minTimeBetweenRotations;
    public float maxTimeBetweenRotations;
    public float minDistance;
    public float maxDistance;
    public float fadeTime;
    public AnimationCurve fadeCurve;

    private AudioClip clip;
    private AudioSource audio;

    private float timer;
    private bool isPlaying;
    private Transform player;
    private float maxVolume;
    private float waitTime;


    void Start()
    {
        audio = GetComponent<AudioSource>();
        Init();
    }


    void Update()
    {
        if(!isPlaying) return;
        
        // increment timer; check if we should rotate
        timer += Time.deltaTime;
        if(timer < waitTime) return;

        // reset timer
        timer = 0f;
        MakeRandomRotation();
    }


    public void AssignClip(AudioClip clip, float timeBetweenRotations, float fadeInTime, float maxVolume, AnimationCurve fadeCurve)
    {
        this.clip = clip;
        if(audio == null) {
            Init();
        }
        audio.clip = clip;
        audio.volume = 0;
        MakeRandomRotation();
        audio.Play();

        fadeTime = fadeInTime;
        this.fadeCurve = fadeCurve;
        this.maxVolume = maxVolume;

        StartCoroutine("FadeIn");
        isPlaying = true;
    }


    public void StartSomePops()
    {
        audio.volume = 0;
        MakeRandomRotation();
        audio.Play();
        isPlaying = true;
        StartCoroutine("FadeIn");
    }


    void MakeRandomRotation()
    {
        transform.position = player.position;
        transform.position = transform.position + transform.forward * Random.Range(minDistance, maxDistance);

        float rotationAmount = Random.Range(0, 359);
        transform.RotateAround(player.position, Vector3.up, rotationAmount);

        waitTime = Random.Range(minTimeBetweenRotations, maxTimeBetweenRotations);
    }


    void Init()
    {
        audio = gameObject.GetComponent<AudioSource>();
        audio.loop = true;
        audio.playOnAwake = false;
        audio.spatialBlend = 1.0f;
        audio.minDistance = 2.0f;
        audio.maxDistance = 10.0f;

        maxVolume = audio.volume;
        // maxVolume = 0.5f;
        isPlaying = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }


    IEnumerator FadeIn()
    {
        float timer = 0;
        while(timer < fadeTime) {
            timer += Time.deltaTime;
            audio.volume = fadeCurve.Evaluate(Mathf.Lerp(0, maxVolume, timer / fadeTime));
            yield return null;
        }

        audio.volume = maxVolume;
    }
}
