using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    [System.Serializable]
    public class CachedTransformData
    {
        public Transform transformData;

        public Vector3 position { get; set; }
        public Quaternion rotation { get; set; }
        public Vector3 scale { get; set; }

        void Start()
        {
            position = transformData.position;
            rotation = transformData.rotation;
            scale    = transformData.localScale;
        }
    }
}

