using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnWait : MonoBehaviour
{
    public UnityEvent events;
    public float waitTime;


    void Start()
    {
        Invoke("InvokeEvent", waitTime);
    }
    

    void InvokeEvent()
    {
        events.Invoke();
    }
}
