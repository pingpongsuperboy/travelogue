using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadScene : MonoBehaviour
{
    public string sceneName;
    public string sceneAlias;

    public GameObject loadingScreen;

    public AudioSource AS;
    public AudioClip portal;

    public void LoadSceneByName()
    {
        jsch.GameManager.instance.LoadSceneBySceneName(sceneName);
    }

    public void LoadSceneByAlias()
    {
        loadingScreen.SetActive(true);
        AS.PlayOneShot(portal, 1.0f);
        Invoke("loadScreen", 2.0f);
    }

    void loadScreen(){
        
        jsch.GameManager.instance.LoadSceneByAlias(sceneAlias);
    }
}
