using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch 
{
    [System.Serializable]
    public class MinMax
    {
        public float min;
        public float max;

        public float GetRandomValue()
        {
            return Random.Range(min, max);
        }
    }
    
}
