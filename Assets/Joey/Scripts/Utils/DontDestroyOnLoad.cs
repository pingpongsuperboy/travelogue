using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch.Utils
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}

