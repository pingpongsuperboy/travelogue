using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace jsch
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class AIController : MonoBehaviour
    {
        // ---------- Public vars -------------------------- //
        public Transform startingDestination;
        public bool isFollowingPlayer;


        // ---------- Private vars -------------------------- //
        Transform player;
        NavMeshAgent agent;



        // ============================================================================= // 
        // ==========    Lifetime Methods         ====================================== //
        // ============================================================================= // 

        void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
        }


        void Start()
        {
            if(startingDestination != null) {
                agent.SetDestination(startingDestination.position);
            }

            if(isFollowingPlayer) {
                FollowPlayer();
            }
        }


        void Update()
        {
            if(isFollowingPlayer) {
                SetDestination(player);
            }
        }




        // ============================================================================= // 
        // ==========    Main Methods         ========================================== //
        // ============================================================================= //

        public void SetDestination(Transform destination)
        {
            agent.SetDestination(destination.position);
        }


        public void SetDestination(string destination)
        {
            // stop following the player if we were doing that
            isFollowingPlayer = false;

            // fetch transform from waypoint manager
            Transform destinationTransform = AIWaypointManager.instance.GetWaypoint(destination);

            // set destination
            SetDestination(destinationTransform);
        }


        public void GoToPlayer()
        {
            isFollowingPlayer = false;
            player = AIWaypointManager.instance.GetPlayerWaypoint();
        }


        public void FollowPlayer()
        {
            isFollowingPlayer = true;
            player = AIWaypointManager.instance.GetPlayerWaypoint();
        }


        public void StopFollowingPlayer()
        {
            isFollowingPlayer = false;
        }
    }
}

