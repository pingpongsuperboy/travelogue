using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    public class AIWaypointManager : MonoBehaviour
    {
        // ---------- Helper classes ----------------------- //
        [System.Serializable]
        public class Waypoint
        {
            public string id;
            public Transform position;
        }



        // ---------- Public vars -------------------------- //
        [Tooltip("List of waypoints in the scene, with their IDs you'll reference by name. No need to include the player in here!")]
        public Waypoint[] waypoints;



        // ---------- Private vars -------------------------- //
        Dictionary<string, Transform> waypointLookup;
        Waypoint playerWaypoint;

        // singleton instance
        public static AIWaypointManager instance;




        // ============================================================================= // 
        // ==========    Lifetime Methods         ====================================== //
        // ============================================================================= // 

        void Awake()
        {
            // ensure singleton instance 
            if(AIWaypointManager.instance != null && AIWaypointManager.instance != this) {
                Destroy(this.gameObject);
            }
            else {
                AIWaypointManager.instance = this;
            }

            // initialize player waypoint
            playerWaypoint = new Waypoint();
            playerWaypoint.id = "player";
            playerWaypoint.position = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

            // create waypoint lookup
            waypointLookup = new Dictionary<string, Transform>();
            foreach(var waypoint in waypoints) {
                waypointLookup.Add(waypoint.id.ToLower(), waypoint.position);
            }

            // for ease of use, add player to lookup
            waypointLookup.Add("player", playerWaypoint.position);
        }




        // ============================================================================= // 
        // ==========    Main Methods         ========================================== //
        // ============================================================================= //

        public Transform GetWaypoint(string id) {
            // if we have that id, return the transform
            if(waypointLookup.ContainsKey(id)) {
                return waypointLookup[id];
            }

            // if not, throw error and return null
            Debug.LogError($"tried to get waypoint for id {id} but no such waypoint is known...did you make a typo, or else forget to add a waypoint to the global list?");
            return null;
        }


        public Transform GetPlayerWaypoint()
        {
            return playerWaypoint.position;
        }
    }

}
