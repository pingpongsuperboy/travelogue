using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToRPGOnClick : MonoBehaviour
{
    public string targetTag = "MagicStone";
    public float maxDistance = 3;
    public GameObject youGotStoneCanvas;

    
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && IsLookingAtTarget()) {
            youGotStoneCanvas.SetActive(true);
        }
    }


    bool IsLookingAtTarget()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit,  maxDistance)) {
            if(hit.transform.tag == targetTag) {
                return true;
            }
        }

        return false;
    }
}
