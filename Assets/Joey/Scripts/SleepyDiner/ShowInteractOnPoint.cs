using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowInteractOnPoint : MonoBehaviour
{
    public string targetTag = "MagicStone";
    public GameObject iconOnLookAt;
    public float maxDistance = 3;

    
    void Update()
    {
        if(IsLookingAtTarget())
            iconOnLookAt.SetActive(true);
        else
            iconOnLookAt.SetActive(false);   
    }


    bool IsLookingAtTarget()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit,  maxDistance)) {
            if(hit.transform.tag == targetTag) {
                return true;
            }
        }

        return false;
    }
}
