using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jsch
{
    public class Dance : MonoBehaviour
    {
        [System.Serializable]
        public class TransformData 
        {
            public Vector3 localTranslation;
            public Vector3 localRotation;
        }

        public MinMax standingAndWaitingTime;
        public MinMax dancingInterval;
        public float jumpingInterval;

        public TransformData danceLeft;
        public TransformData danceRight;
        public float jumpHeight;
        [Range(0,1)]
        public float jumpingOdds;
        public bool startOnAwake = false;


        private Vector3 startingPosition;
        private Quaternion startingRotation;


        void Start()
        {
            startingPosition = transform.position;
            startingRotation = transform.rotation;

            if(startOnAwake)
                WaitThenJumpOrDance();
        }

        public void StartJumping()
        {
            WaitThenJumpOrDance();
        }



        bool ShouldJump()
        {
            return Random.Range(0.0f,1.0f) < jumpingOdds;
            Invoke("StandThenWait", dancingInterval.GetRandomValue());
        }


        void Jump()
        {
            transform.Translate(Vector3.up * jumpHeight);
            Invoke("StandThenWait", jumpingInterval);
        }


        void DanceLeft()
        {
            transform.Translate(danceLeft.localTranslation);
            transform.Rotate(danceLeft.localRotation);
            Invoke("DanceRight", dancingInterval.GetRandomValue());
        }


        void DanceRight()
        {
            Stand();
            transform.Translate(danceRight.localTranslation);
            transform.Rotate(danceRight.localRotation);
            Invoke("StandThenWait", dancingInterval.GetRandomValue());
        }


        void Stand()
        {
            transform.position = startingPosition;
            transform.rotation = startingRotation;
        }


        void StandThenWait()
        {
            Stand();
            WaitThenJumpOrDance();
        }


        void WaitThenJumpOrDance()
        {
            if(ShouldJump())
                Invoke("Jump", standingAndWaitingTime.GetRandomValue());
            else
                Invoke("DanceLeft", standingAndWaitingTime.GetRandomValue());
        }
    }
}

