using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour
{
    public float rotationSpeed;
    public bool rotateOnAwake = true;


    bool isRotating;


    void Start()
    {
        isRotating = rotateOnAwake;
    }


    void Update()
    {
        if(!isRotating) return; // wait to rotate

        transform.RotateAround(transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
    }


    public void StartRotating()
    {
        isRotating = true;
    }
}
