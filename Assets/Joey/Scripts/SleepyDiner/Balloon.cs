using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Balloon : MonoBehaviour
{
    public float minBuoyancy;
    public float maxBuoyancy;
    public ForceMode forceMode;

    float buoyancy;
    Rigidbody rigidbody;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        buoyancy = Random.Range(minBuoyancy, maxBuoyancy);
    }

    void FixedUpdate()
    {
        rigidbody.AddForce(Vector3.up * buoyancy, forceMode);
    }
}
