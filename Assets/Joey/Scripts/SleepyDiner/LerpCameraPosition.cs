using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpCameraPosition : MonoBehaviour
{
    public FadeInOutBlack fadeScript;
    public Transform endTransform;
    public AnimationCurve animationCurve;

    private Vector3 startPosition;
    private Vector3 endPosition;
    private Quaternion startRotation;
    private Quaternion endRotation;


    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        endPosition = endTransform.position;
        endRotation = endTransform.rotation;
    }


    void Update()
    {
        float lerp = animationCurve.Evaluate(fadeScript.GetLerpValue());
        Vector3 position = Vector3.Lerp(startPosition, endPosition, lerp);
        Quaternion rotation = Quaternion.Lerp(startRotation, endRotation, lerp);

        transform.position = position;
        transform.rotation = rotation;
    }
}
