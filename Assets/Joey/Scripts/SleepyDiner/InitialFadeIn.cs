using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Audio;


namespace jsch.Diner
{
    public class InitialFadeIn : MonoBehaviour
    {
        public AnimationCurve fadeCurve;
        public UnityEvent eventOnAwake;

        public AudioMixer audioMixer;
        public AnimationCurve animationCurve;

        
        public AnimationCurve camAnimationCurve;

        private float lowPassStart = 22000;
        public float lowPassEnd = 1000;
        private float volumeStart = 0;
        private float volumeEnd = -14.1f;
        private string lowpassParameterName = "Lowpass";
        private string volumeParameterName = "Volume";
        
        private float alphaLerp;
        private float mouseShakePower = 0.62f; // taken from the other script
        

        private Vector3 startPosition;
        private Vector3 endPosition;
        private Quaternion startRotation;
        private Quaternion endRotation;

        
        SpriteRenderer image;

        void Start()
        {
            image = GetComponent<SpriteRenderer>();
            alphaLerp = 1;

            startPosition = transform.position;
            startRotation = transform.rotation;
        }

        void Update()
        {
            // wake up if mouse shaking (check both axes)
            alphaLerp -= mouseShakePower * Time.deltaTime * Mathf.Abs(Input.GetAxis("Mouse X"));
            alphaLerp -= mouseShakePower * Time.deltaTime * Mathf.Abs(Input.GetAxis("Mouse Y"));

            alphaLerp = Mathf.Max(alphaLerp, 0); // make sure it never goes below zero

            // update image color
            Color color = image.color;
            color.a = fadeCurve.Evaluate(alphaLerp);
            image.color = color;

            // update audio
            float lerp = animationCurve.Evaluate(alphaLerp);
            float lowpass = Mathf.Lerp(lowPassStart, lowPassEnd, lerp);
            float volume = Mathf.Lerp(volumeStart, volumeEnd, lerp);

            audioMixer.SetFloat(lowpassParameterName, lowpass);
            audioMixer.SetFloat(volumeParameterName, volume);

            if(IsAwake()) {
                // make sure alpha is all the way up
                color.a = 1.0f;
                image.color = color;

                // invoke awake events
                eventOnAwake.Invoke();

                // disable ourselves
                Destroy(this.gameObject);
            }
        }


        bool IsAwake()
        {
            return image.color.a < float.Epsilon;
        }

        public float GetLerpValue()
        {
            return alphaLerp;
        }
    }

}
