using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Diner
{
    public class CrowdBanter : MonoBehaviour
    {
        public TMP_Text[] textMeshes;
        public string[] banter;
        public jsch.MinMax timeBetweenBanter;
        public float timeToShowBanter;


        float timer;
        bool isRunning;
        float targetTime;


        void Start()
        {
            timer = 0;
            isRunning = false;
            ClearText();
        }


        public void BeginBanter()
        {
            isRunning = true;
        }


        void Update()
        {
            if(!isRunning) return; // haven't started

            timer += Time.deltaTime;            
            if(timer < timeToShowBanter) return; // keep waiting

            // they've stopped speaking, hide their text
            ClearText();
            if(timer < targetTime) return; // keep waiting

            SayRandomBanter();
            SetTargetTime();            
            timer = 0;
        }


        void ClearText()
        {
            foreach(var tmp in textMeshes) {
                tmp.text = "";
            }
        }


        void SayRandomBanter()
        {
            var textMesh = textMeshes[Random.Range(0, textMeshes.Length)];
            var text = banter[Random.Range(0, banter.Length)];
            textMesh.text = text;
        }


        void SetTargetTime()
        {
            targetTime = timeBetweenBanter.GetRandomValue();
        }
    }
}

