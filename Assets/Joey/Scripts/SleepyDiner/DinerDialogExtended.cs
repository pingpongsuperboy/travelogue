using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Diner
{
    public class DinerDialogExtended : MonoBehaviour
    {
        [System.Serializable]
        public class Dialog
        {
            public int personTalking;
            public string text;
            public float waitTime;
            public bool clearAfterWait = false;
        }


        public Dialog[] sceneDialog;
        public TMP_Text[] textMeshProsInScene;
        public float initialWaitTime = 2.0f;
        public bool alwaysClearDialog = true;

        private int dialogIndex;
        private bool isTyping;


        void Start()
        {
            dialogIndex = 0;
            isTyping = false;

            foreach(var dialog in textMeshProsInScene) {
                dialog.text = "";
            }

            StartCoroutine("ExecuteDialog");
        }



        IEnumerator ExecuteDialog()
        {
            yield return new WaitForSeconds(initialWaitTime);
            while(dialogIndex < sceneDialog.Length) {
                Dialog dialog = sceneDialog[dialogIndex];
                ShowDialog(dialog);
                yield return new WaitForSeconds(dialog.waitTime);

                // if(dialog.clearAfterWait) {
                if(alwaysClearDialog || dialog.clearAfterWait) {
                    ClearDialog(dialog);
                }

                dialogIndex++;
            }
        }


        void ShowDialog(Dialog dialog)
        {
            textMeshProsInScene[dialog.personTalking].text = dialog.text;
        }


        void ClearDialog(Dialog dialog)
        {
            textMeshProsInScene[dialog.personTalking].text = "";
        }
    }
}

