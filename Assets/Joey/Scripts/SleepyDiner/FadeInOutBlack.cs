using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FadeInOutBlack : MonoBehaviour
{
    public AnimationCurve fadeCurve;
    [Range(0,5)]
    public float fadeToBlackSpeed = 0.1f;
    [Range(0, 1)]
    public float mouseShakePower = 1.0f;

    public float pauseAtFullAwake = 1.0f;
    [Range(0,5)]
    public float maxFadeToBlackSpeed = 0.7f;
    [Range(0,1)]
    public float fadeSpeedIncreaseRate = 0.05f;

    public UnityEvent eventOnAsleep;

    public float minTimeBeforeFullyAsleep = 15;
    public float wakeUpSpeed = 1.0f;

    Image image;
    float awakeTimer;
    float alphaLerp;
    float timer;
    bool awakingByForce;

    void Start()
    {
        image = GetComponent<Image>();
        awakeTimer = 0;
        alphaLerp = 0;
        timer = 0;
        awakingByForce = false;
    }


    void Update()
    {
        timer += Time.deltaTime;
        if(awakingByForce) {
            WakeUpByForce();
            return;
        }
        IncreaseSleepiness();

        // if we're awake, just wait for next frame
        if(IsAwake()) return;

        // start falling asleep
        alphaLerp += fadeToBlackSpeed * Time.deltaTime;

        // wake up if mouse shaking (check both axes)
        alphaLerp -= mouseShakePower * Time.deltaTime * Mathf.Abs(Input.GetAxis("Mouse X"));
        alphaLerp -= mouseShakePower * Time.deltaTime * Mathf.Abs(Input.GetAxis("Mouse Y"));

        alphaLerp = Mathf.Max(alphaLerp, 0); // make sure it never goes below zero

        // update image color
        Color color = image.color;
        color.a = fadeCurve.Evaluate(alphaLerp);
        image.color = color;

        if(!IsFullyAsleep()) return; // wait to be fully asleep, then trigger asleep event

        // if we shouldn't fully fall asleep yet, wake back up
        if(timer < minTimeBeforeFullyAsleep) {
            awakingByForce = true;
            return;
        }
        eventOnAsleep.Invoke();
        gameObject.SetActive(false);
    }

    public void FadeIn()
    {
        StartCoroutine("FadeBackgroundIn");
    }

    public void FadeOut()
    {
        StartCoroutine("FadeBackgroundOut");
    }

    public float GetLerpValue()
    {
        return alphaLerp;
    }


    void WakeUpByForce()
    {
        alphaLerp -= wakeUpSpeed * Time.deltaTime;
        alphaLerp = Mathf.Max(alphaLerp, 0); // make sure it never goes below zero


        // update image color
        Color color = image.color;
        color.a = fadeCurve.Evaluate(alphaLerp);
        image.color = color;

        if(alphaLerp <= float.Epsilon) {
            awakingByForce = false;
        }
    }


    bool IsAwake()
    {
        // if we're late into the game, can't be awake
        if(timer > 60) return false;

        // if we haven't gotten the alpha to zero, we're not awake
        if(image.color.a > float.Epsilon) return false;

        // if our awake timer has run out, we're not awake
        if(awakeTimer >= pauseAtFullAwake) {
            // reset timer for next time
            awakeTimer = 0;
            return false;
        }

        // otherwise, increment awaketimer, return true
        awakeTimer += Time.deltaTime;
        return true;
    }

    bool IsFullyAsleep()
    {
        return alphaLerp >= (1.0f - float.Epsilon);
    }

    void IncreaseSleepiness()
    {
        fadeToBlackSpeed = Mathf.Clamp(fadeToBlackSpeed + (fadeSpeedIncreaseRate * Time.deltaTime), 0, maxFadeToBlackSpeed);
        if(timer > 60) {
            mouseShakePower -= 0.03f * Time.deltaTime;
        }
    }


    // IEnumerator FadeBackgroundIn()
    // {
    //     Debug.Log("fading in!");
    //     Color color = Color.black;
    //     color.a = 0;

    //     float timer = 0f;
    //     while(timer < fadeInTime) {
    //         timer += Time.deltaTime;
    //         color.a = Mathf.Lerp(0, 1, timer / fadeInTime);
    //         image.color = color;

    //         yield return null;
    //     }

    //     color.a = 1;
    // }

    // IEnumerator FadeBackgroundOut()
    // {
    //     Debug.Log("fading out!");
    //     Color color = Color.black;
    //     color.a = 1;

    //     float timer = 0f;
    //     while(timer < fadeInTime) {
    //         timer += Time.deltaTime;
    //         color.a = 1.0f - Mathf.Lerp(0, 1, timer / fadeInTime);
    //         image.color = color;

    //         yield return null;
    //     }

    //     color.a = 0;
    // }
}
