using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class LerpAudio : MonoBehaviour
{
    public AudioMixer audioMixer;
    public FadeInOutBlack fadeScript;

    public float lowPassStart;
    public float lowPassEnd;
    public float volumeStart;
    public float volumeEnd;
    public AnimationCurve animationCurve;
    public string lowpassParameterName = "Lowpass";
    public string volumeParameterName = "Volume";


    void Update()
    {
        float lerp = animationCurve.Evaluate(fadeScript.GetLerpValue());
        float lowpass = Mathf.Lerp(lowPassStart, lowPassEnd, lerp);
        float volume = Mathf.Lerp(volumeStart, volumeEnd, lerp);

        audioMixer.SetFloat(lowpassParameterName, lowpass);
        audioMixer.SetFloat(volumeParameterName, volume);
    }
}
