using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Diner
{
    public class DinerDialog : MonoBehaviour
    {
        public enum PersonTalking { person1, person2 }

        [System.Serializable]
        public class Dialog
        {
            public PersonTalking personTalking;
            public string text;
            public float waitTime;
            public bool clearAfterWait = false;
        }


        public Dialog[] sceneDialog;
        public TMP_Text person1TextMeshPro;
        public TMP_Text person2TextMeshPro;
        public float initialWaitTime = 2.0f;
        public bool alwaysClearDialog = true;
        public float pauseBetweenLines = 0.0f;
        public GameObject textBackground;

        private int dialogIndex;
        private bool isTyping;


        void Start()
        {
            dialogIndex = 0;
            isTyping = false;

            StartCoroutine("ExecuteDialog");
            textBackground.SetActive(false);
        }



        IEnumerator ExecuteDialog()
        {
            yield return new WaitForSeconds(initialWaitTime);
            while(dialogIndex < sceneDialog.Length) {
                Dialog dialog = sceneDialog[dialogIndex];
                ShowDialog(dialog);
                yield return new WaitForSeconds(dialog.waitTime);

                // if(dialog.clearAfterWait) {
                if(alwaysClearDialog || dialog.clearAfterWait) {
                    ClearDialog(dialog);
                }

                yield return new WaitForSeconds(pauseBetweenLines);

                dialogIndex++;
            }
        }


        void ShowDialog(Dialog dialog)
        {
            if(dialog.personTalking == PersonTalking.person1) {
                person1TextMeshPro.text = dialog.text;
            }
            else {
                person2TextMeshPro.text = dialog.text;
            }
            
            textBackground.SetActive(true);
        }


        void ClearDialog(Dialog dialog)
        {
            if(dialog.personTalking == PersonTalking.person1) {
                person1TextMeshPro.text = "";
            }
            else {
                person2TextMeshPro.text = "";
            }

            textBackground.SetActive(false);
        }
    }
}

