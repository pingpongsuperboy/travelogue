using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class ShowHideMouseShake : MonoBehaviour
{
    public FadeInOutBlack fadeScript;
    [Range(0,1)]
    public float threshold;

    private SpriteRenderer icon;
    private Color showColor;
    private Color hideColor;


    void Start()
    {
        icon = GetComponent<SpriteRenderer>();
        showColor = icon.color;
        hideColor = icon.color;
        hideColor.a = 0;
    }


    void Update()
    {
        // show icon
        if(fadeScript.GetLerpValue() > threshold) {
            icon.color = showColor;
        }   
        // hide icon
        else {
            icon.color = hideColor;
        }
    }
}
