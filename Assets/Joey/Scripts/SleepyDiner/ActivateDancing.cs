using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateDancing : MonoBehaviour
{
    public void StartDancing()
    {
        var danceScripts = transform.GetComponentsInChildren<jsch.Dance>();
        foreach(var danceScript in danceScripts) {
            danceScript.StartJumping();
        }
    }
}
