using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperFlipController : MonoBehaviour
{
    public float minSpeed;
    public float maxSpeed;
    public float minWaitBetweenClips;
    public float maxWaitBetweenClips;
    public float pitchFluctuation;
    public float heightFluctuation;
    public AudioClip[] paperClips;


    AudioSource audio;
    float speed;
    float timer;
    Vector3 startingPosition;


    void Start()
    {
        audio = GetComponent<AudioSource>();
        timer = 0;
        startingPosition = transform.position;

        SetRandomSpeed();
        StartCoroutine("PlayPaperSounds");
    }


    void Update()
    {

        // move down
        transform.Translate(Vector3.down * speed * Time.deltaTime);

        // reset position if we're below ground
        if(transform.position.y < 0) {
            transform.position = startingPosition;
            SetRandomSpeed();
        }
    }


    IEnumerator PlayPaperSounds()
    {
        while(true) {
            PlayRandomClip();
            SetRandomHeight();
            yield return new WaitForSeconds(audio.clip.length);
            float waitTime = Random.Range(minWaitBetweenClips, maxWaitBetweenClips);
            yield return new WaitForSeconds(waitTime);
        }
    }



    void SetRandomSpeed()
    {
        speed = Random.Range(minSpeed, maxSpeed);
    }


    void PlayRandomClip()
    {
        // make sure we're not playing
        audio.Stop();

        // set random clip
        int clip = Random.Range(0, paperClips.Length);
        audio.clip = paperClips[clip];

        // set pitch
        audio.pitch = Random.Range(1.0f - pitchFluctuation, 1.0f + pitchFluctuation);

        // play the clip
        audio.Play();
    }


    void SetRandomHeight()
    {
        Vector3 pos = startingPosition;
        pos.y += Random.Range(-heightFluctuation, heightFluctuation);
        transform.position = pos;
    }
}
