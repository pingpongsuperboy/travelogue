using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkCarrot : MonoBehaviour
{
    public float blinkTime = 1.0f;
    
    Image image;
    float timer;
    Color showColor;
    Color hideColor;
    bool isShowing;


    void Start()
    {
        timer = 0;
        isShowing = true;
        
        image = GetComponent<Image>();
        showColor = image.color;
        hideColor = image.color;
        hideColor.a = 0;
    }


    void Update()
    {
        timer += Time.deltaTime;
        if(timer < blinkTime) return; // keep waiting
        
        image.color = isShowing? showColor : hideColor;
        isShowing = !isShowing;
        timer = 0;
    }
}
