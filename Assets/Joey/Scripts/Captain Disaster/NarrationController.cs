using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace jsch.Menu.CaptainDisaster
{
    public class NarrationController : MonoBehaviour
    {
        public string[] narration;
        public TMP_Text textMesh;
        public GameObject panel;
        public GameObject carrot;
        public float pauseBeforeAllowingContinue;
        public float pauseBetweenLines;
        public string nextSceneToLoad = "Train";


        enum NarrationState { WaitingForInput, PausingBetweenLines }


        float timer;
        float targetTime;
        int narrationIndex;
        NarrationState narrationState;

        void Start()
        {
            timer = 0;
            narrationIndex = 0;
            targetTime = pauseBetweenLines;
            narrationState = NarrationState.PausingBetweenLines;
            HideNarration();
        }


        void Update()
        {
            timer += Time.deltaTime;
            if(timer < targetTime) return; // keep waiting

            switch(narrationState) {
                case NarrationState.PausingBetweenLines:
                    if(narrationIndex >= narration.Length) {
                        jsch.GameManager.instance.LoadSceneByAlias(nextSceneToLoad);
                        Destroy(this);
                        return;
                    }

                    ShowNarration();
                    narrationIndex++;
                    timer = 0;
                    targetTime = pauseBeforeAllowingContinue;
                    narrationState = NarrationState.WaitingForInput;
                    break;
                
                case NarrationState.WaitingForInput:
                    // make sure carrot is showing
                    if(!carrot.activeInHierarchy) {
                        carrot.SetActive(true);
                    }

                    if(Input.GetMouseButtonDown(0) || Input.anyKeyDown) {
                        HideNarration();
                        timer = 0;
                        targetTime = pauseBetweenLines;
                        narrationState = NarrationState.PausingBetweenLines;
                    }
                    
                    break;
            }
        }


        void HideNarration()
        {
            textMesh.text = "";
            panel.SetActive(false);
            carrot.SetActive(false);
        }


        void ShowNarration()
        {
            textMesh.text = narration[narrationIndex];
            panel.SetActive(true);
        }
    }
}

