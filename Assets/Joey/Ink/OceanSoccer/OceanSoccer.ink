# triggerOn: deck
- GOTO(on_deck)

#triggerOn: sailor1
#flow: georgeOpening
- GOTO(sailor1_opening)




=== sailor1_opening ===
# flow: georgeOpening
# playImmediately: true
# char: sailor1
# wait: 2
- Hey Archie, the game is starting!
# wait: 3.0
# clear: true
- We're at the front of the boat, come find us!
# wait: 1.5
# clear: true
- Okay bye!
- SET_DESTINATION(radio)

-> end


=== on_deck ===
# playImmediately: true
# char: sailor2
# wait: 2
# interrupt: true
- Oh hey there, welcome outside!


-> end


=== on_repair ===
# flow: repair
# playImmediately: true
# char: capn
- SET_DESTINATION(radio_tower)

# wait: 2.0
- Those boys and their soccer...

# wait: 2.0
- Well, come on then.

# wait: 2.0
- They're good lads, really.

# wait: 2.0
- And the games remind them of home, you know.

# wait: 3.0
- Besides the soccer games, they make Squid Bolognese on Tuesdays, the way they grew up.

# wait: 2.5
- When I first started this job, years and years ago I used to -- wrench please.

# wait: 1.0
- Thank you.

# wait: 2.0
- I used to write letters to friends, every day.

# wait: 2.5
- I used to write so many that -- screw driver.

# wait: 1.0
- Thank you.

# wait: 2.0
- -- so many that my desk became filled with them.

# wait: 3.0
- And when the shipping trip ended, when we finally came back to land, I would gather them in my arms to send out.

# wait: 2.5
- But I was afraid of how much time had passed.

# wait: 2.5
- I never sent those -- hacksaw.

# wait: 1.5
- -- those letters.

# wait: 2.0
- I wonder where they ended up...

# wait: 2.5
- Anyways, what I'm getting at is that it's important -- hammer.

# wait: 2.0
- It's important we get this fixed.

// here the radio comes back on
# wait: 2.0
- That did it!

# clear: false
- Shall we catch the end of the game?


-> end




=== end ===
-> END