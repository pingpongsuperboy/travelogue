# playImmediately: true
- GOTO(diner_convo)


=== diner_convo ===
# playImmediately: true
# char: person2
# wait: 3
- ...
# char: person1
# wait: 2
- Did you see last night's "Love Under Fire" episode?

# char: person2
# wait: 3
- I missed it!

# char: person1
# wait: 2
- Wow. So you know Gina.

# char: person2
# wait: 2
- Of course.

# char: person1
# wait: 2
- This episode she's under fire, so they take her out to a volcano.

# char: person2
# wait: 2
- Nice.

# char: person1
# wait: 2
- And Billy's at the bottom all like, "You can do it Gina!"

# char: person2
# wait: 2
- Love Billy.

# char: person1
# wait: 2
- Yes, but Gina's tightroping over the colcano, arms wobbling--

# char: person2
# wait: 2
- Oh no.

# char: person1
# wait: 2
- yeah. Yeah. And she falls.

# char: person2
# wait: 2
- Oh man oh man.

# char: person1
# wait: 2
- She falls far, but everyone below gets the little mattress catcher thing

# char: person2
# wait: 2
- Thank god.

# char: person1
# wait: 2
- And they catch her.

# char: person2
# wait: 2
- Is she okay?

# char: person1
# wait: 2
- She's okay. She's fine. I mean, she's scared.

# char: person2
# wait: 2
- Of course.

# char: person1
# wait: 2
- But she's alright. And she spends a long time lying there and then gets up and walks away.

# char: person2
# wait: 2
- Wow.

# char: person1
# wait: 2
- And that night, at dinner, she says she's leaving.

# char: person2
# wait: 2
- She's leaving?!

# char: person1
# wait: 2
- She says that while she was lying there, after she was certain she would die...

# char: person1
# wait: 2
- She said she never felt more cared for.

# char: person2
# wait: 2
- Aww Gina...

# char: person1
# wait: 2
- She said she realized that the kind of love she's been seeking, it's the kind that sees and acts on what's broken inside of her.

# char: person2
# wait: 2
- But they just saved her!

# char: person1
# wait: 2
- She said they're all wonderful, really, but they're all as lost as she is.

# char: person2
# wait: 2
- Gina!!

# char: person1
# wait: 2
- And then she left as the goofy little "Love Under Fire!" theme ran.

# char: person2
# wait: 2
- Wow.




-> end 

=== end ===
-> END