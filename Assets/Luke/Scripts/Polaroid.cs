using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polaroid : MonoBehaviour
{
    public GameObject player;

    public bool playing = false;
    public float cd = 3.0f;
    public float cdVal;

    AudioSource AS;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        AS = transform.GetChild(2).gameObject.GetComponent<AudioSource>();
        cdVal = cd;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, player.transform.position)<=5.0f && player.GetComponent<Player>().lookAtFilm){
            transform.GetChild(0).gameObject.SetActive(true);
            //transform.GetChild(1).gameObject.SetActive(true);
            if(!playing){
                AS.enabled = true;
                playing = true;
            }

        }


        if(!AS.isPlaying){
            AS.enabled = false;
            transform.GetChild(0).gameObject.SetActive(false);
        }

        if(playing && !AS.isPlaying){
            cd -= 0.5f*Time.deltaTime;
        }

        if(cd <= 0){
            playing = false;
            cd = cdVal;
        }
    }

    IEnumerator lightOff(){
        yield return new WaitForSeconds(1f);
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
    }


}
