using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public GameObject filmDetector;
    public GameObject film;
    public GameObject filmSound;
    public GameObject bgm;
    public GameObject stairs;
    public GameObject tv;

    public GameObject target;
    public GameObject target2;
    public GameObject fadeOut;
    public GameObject museumManager;
    public bool lookAtFilm = false;
    public bool lookAtTv = false;
    public bool inTransition;
    
    Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.z <= 100){
            if(GetComponent<FirstPersonDrifter>().walkSpeed >2){
                GetComponent<FirstPersonDrifter>().walkSpeed -= 0.1f*Time.deltaTime;
            }
            if(transform.GetChild(0).gameObject.transform.localPosition.y>0){
                transform.GetChild(0).gameObject.transform.localPosition = new Vector3(0, 0, 0);
            }
            
            
        }
        RaycastHit hit;
        Vector3 cameraCenter = camera.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, camera.nearClipPlane));
        if (Physics.Raycast(cameraCenter, this.transform.forward, out hit, 1000))
        {
            GameObject obj = hit.transform.gameObject;
            if(obj.tag == "polaroid"){
                lookAtFilm = true;
            }
            else{
                lookAtFilm = false;
            }
        }

        if(Physics.Raycast(cameraCenter, this.transform.forward, out hit, 1000)){

            GameObject obj = hit.transform.gameObject;
            if(obj.tag == "tv"){
                lookAtTv = true;
            }
            else{
                lookAtTv = false;
            }
        }

        if(Vector3.Distance(transform.position, tv.transform.position)<=3.0f && lookAtTv && !inTransition){
            if (Input.GetMouseButtonDown(0)){
               inTransition = true;
               StartCoroutine(toWaterScene());
            }

        }
    }

    IEnumerator toWaterScene(){
        GetComponent<FirstPersonDrifter>().enabled = false;
        GetComponent<MouseLook>().enabled = false;
        Camera.main.GetComponent<MouseLook>().enabled = false;
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 1.0f*Time.deltaTime);
        Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, target2.transform.rotation, 1f*Time.deltaTime);
        yield return new WaitForSeconds(1.0f);
        Camera.main.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        fadeOut.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(3.0f);
        museumManager.GetComponent<MuseumManager>().changeScene = true;

    }

    // void OnTriggerEnter(Collider coll){
    //     if (coll.gameObject.tag == "film"){
    //         film.GetComponent<Animator>().enabled = true;
    //         film.GetComponent<Animator>().speed = 1;
    //     }
    // }

    // void OnTriggerExit(Collider coll){
    //     if (coll.gameObject.tag == "film"){
    //         film.GetComponent<Animator>().speed = 0;
    //     }        
    // }
}
