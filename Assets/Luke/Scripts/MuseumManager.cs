using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuseumManager : MonoBehaviour
{
    public bool changeScene;

    public GameObject museum;
    public GameObject water;
    public GameObject fade;
    public GameObject player;
    public GameObject mainCam;

    public GameObject paperAudio;
    public GameObject waterCam;

    public bool stoneReceived;
    public GameObject tvInteractObject;
    public GameObject gotStoneObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(changeScene){
            museum.SetActive(false);
            water.SetActive(true);
            player.GetComponent<Player>().enabled = false;
            player.GetComponent<MouseLook>().enabled = false;
            player.GetComponent<FirstPersonDrifter>().enabled = false;
            mainCam.SetActive(false);
            fade.GetComponent<Animator>().SetBool("fadeIn", true);
            tvInteractObject.SetActive(false);

            globalSound();
            StartCoroutine(getStone());

        }

        if(stoneReceived){
            waterCam.GetComponent<Animator>().enabled = true;
            
        }

        if(waterCam.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1){
            
            museumFinish();
        }
    }

    void globalSound(){
        foreach (Transform sub in paperAudio.transform){
            sub.gameObject.GetComponent<AudioSource>().spatialBlend = 0;
        }
    }

    IEnumerator getStone(){
        yield return new WaitForSeconds(2.0f);
        stoneReceived = true;
    }

    void ShowGotStone()
    {
        fade.GetComponent<Image>().enabled = false;
        gotStoneObject.SetActive(true);
        Invoke("GoToRPG", 5);
    }


    void GoToRPG()
    {
        jsch.GameManager.instance.ReturnToMenu();
    }

    void museumFinish(){
        fade.GetComponent<Animator>().SetBool("fadeIn", false);
        foreach (Transform sub in paperAudio.transform){
            sub.gameObject.GetComponent<AudioSource>().volume -= 0.1f*Time.deltaTime;
        }

        Invoke("ShowGotStone", 5);
    }
}


