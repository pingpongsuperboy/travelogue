using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheaterCamera : MonoBehaviour
{
    public bool left;
    public bool center;
    public bool right;

    public bool noNoise = true;
    public bool movieStart = false;

    public float leftLookTime = 0f;

    public bool poked = false;
    public bool shush = false;


    public GameObject noise;
    public GameObject aud1;
    public GameObject aud2;

    public GameObject shushHand;

    public float finalCD = 210;

    // Start is called before the first frame update
    void Start()
    {
        aud1 = GameObject.Find("Aud1");
        aud2 = GameObject.Find("Aud2");
    }

    // Update is called once per frame
    void Update()
    {
        finalCD -= 1f * Time.deltaTime;
        if(center){
            aud2.GetComponent<Audience>().lookedAt = false;
            if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)){
                Camera.main.transform.rotation = Quaternion.Euler(-0.4f,107,1.5f);
                center = false;
                left = true;
            }

            if(aud2.GetComponent<Audience>().staring && !aud2.GetComponent<Audience>().inTransition){
                aud2.GetComponent<Audience>().backToWatch();
            }
        }

        else if(left){
            aud2.GetComponent<Audience>().lookedAt = true;
            if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)){
                Camera.main.transform.rotation = Quaternion.Euler(-1.5f,180,0);
                left = false;
                center = true;
            }
        }

        if(finalCD <=0 && !aud2.GetComponent<Audience>().inTransition && aud2.GetComponent<Audience>().asleep){
            aud2.GetComponent<Audience>().deepSleep();
            aud2.GetComponent<Audience>().enabled = false;
        }

        // else if(right){
        //     if(Input.GetKeyDown(KeyCode.LeftArrow)){
        //         Camera.main.transform.rotation = Quaternion.Euler(-1.5f,180,0);
        //         right = false;
        //         center = true;
        //     }
        // }


        if(left){
            if(finalCD>0){
                GetComponent<Hand>().getHand = true;
            }
            else if(finalCD <=0 && !aud2.GetComponent<Audience>().inTransition && aud2.GetComponent<Audience>().asleep){
                GetComponent<Hand>().getHand = false;
                GetComponent<Hand>().finalHand = true;

            }


            if(poked){
                wakeUp();
                poked = false;
            }          
        }

        if(this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
        {
            GetComponent<Animator>().enabled = false;
        }


    }

    void wakeUp(){
        // aud1.GetComponent<Audience>().lookState();
        aud2.GetComponent<Audience>().wakeState();
        // yield return new WaitForSeconds(3.0f);
        // aud1.GetComponent<Audience>().watchState();
        // aud2.GetComponent<Audience>().watchState();
        // aud1.GetComponent<Audience>().inTransition = false;
        // aud2.GetComponent<Audience>().inTransition = false;
        
    }

    // void lookToWatch(){
    //     shush = false;
    //     // aud1.GetComponent<Audience>().watchState();
    //     aud2.GetComponent<Audience>().watchState();
    //     GetComponent<Hand>().getHand = false;
        
    // }
    
}
