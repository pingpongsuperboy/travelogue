using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovieLogic : MonoBehaviour
{
    public AudioSource leftAS;
    public AudioSource rightAS;
    public AudioClip ambience;

    public AudioClip sound1;
    public AudioClip sound2;
    public AudioClip sound3;


    public GameObject noise;
    public GameObject archie;
    public GameObject voiceOver;


    public bool movieStart = false;
    public bool movie1 = false;
    public bool movie2 = false;
    public bool movie3 = false;

    public float interval = 27.0f;
    public float voiceOverCD = 157.0f;
    public float intervalVal;
    public float minVolume;
    
    // Start is called before the first frame update
    void Start()
    {
        leftAS = transform.GetChild(0).gameObject.GetComponent<AudioSource>();
        rightAS = transform.GetChild(1).gameObject.GetComponent<AudioSource>();     
        intervalVal = interval;
    }

    // Update is called once per frame
    void Update()
    {
        voiceOverCD -= 1.0f * Time.deltaTime;
        if(voiceOverCD<=0){
            voiceOver.SetActive(true);
            lowerMusicvolume();
        }

        if(voiceOverCD <= -70.0f){
            minVolume = 0.1f;
            lowerMusicvolume();
        }

        if(movieStart){
            interval -= 1f * Time.deltaTime;
            if(interval <= 0){
                leftAS.clip = sound1;
                rightAS.clip = sound1;
                leftAS.Play();
                rightAS.Play();
                movieStart = false;
                Camera.main.GetComponent<TheaterCamera>().movieStart = true;
                Camera.main.GetComponent<TheaterCamera>().enabled = true;
                Camera.main.GetComponent<Hand>().enabled = true;
                archie.GetComponent<Audience>().enabled = true;
            }
        }


    }

    void lowerMusicvolume(){
        if(leftAS.volume >= minVolume){
            leftAS.volume -= 0.1f * Time.deltaTime;
        }
        if(rightAS.volume >= minVolume){
            rightAS.volume -= 0.1f * Time.deltaTime;
        }
        
    }

    IEnumerator startMovie(){
        yield return new WaitForSeconds(3.0f);
        leftAS.clip = sound1;
        rightAS.clip = sound1;
        leftAS.Play();
        rightAS.Play();
        movieStart = false;
    }
}
