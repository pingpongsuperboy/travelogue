using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audience : MonoBehaviour
{

    public bool isTalking;
    public bool isWatching;
    public bool isMad;

    public bool inTransition = false;

    //public GameObject talking;
    public GameObject watching;
    public GameObject sleeping;
    public GameObject woke;
    public GameObject looking;
    public GameObject awkward;
    public GameObject stare;
    public GameObject deepStare;

    public bool asleep;
    public bool staring;
    public bool lookedAt;
    public float sleepCD;



    // Start is called before the first frame update
    void Start()
    {
        watching = transform.GetChild(0).gameObject;
        sleeping = transform.GetChild(1).gameObject;
        woke = transform.GetChild(2).gameObject;
        looking = transform.GetChild(3).gameObject;
        awkward = transform.GetChild(4).gameObject;
        stare = transform.GetChild(5).gameObject;
        deepStare = transform.GetChild(6).gameObject;
        
        sleepCD = Random.Range(3,7);
    }

    // Update is called once per frame
    void Update()
    {

        if(!inTransition && !asleep && sleepCD >0 && !staring && !lookedAt){
            sleepCD -= 0.5f * Time.deltaTime;
        }

        if(sleepCD <= 0 && !inTransition && !asleep && !staring){
            asleep = true;
            sleeping.SetActive(true);
            watching.SetActive(false);
            
            // looking.SetActive(false);
            // woke.SetActive(false);
            // awkward.SetActive(false);
            // stare.SetActive(false);
            // deepStare.SetActive(false);

        }


    }

    public void wakeState(){
        inTransition = true;
        if(asleep){
            StartCoroutine(wakeUp());   
        }
        else if(!staring){
            staring = true;
            StartCoroutine(stareAt());
        }
           
    }

    IEnumerator stareAt(){
        watching.SetActive(false);
        stare.SetActive(true);
        yield return new WaitForSeconds(2f);
        stare.SetActive(false);
        deepStare.SetActive(true);
        yield return new WaitForSeconds(3f);
        inTransition = false;
    }

    IEnumerator wakeUp(){
        sleeping.SetActive(false);
        woke.SetActive(true);
        Debug.Log("waiting");
        yield return new WaitForSeconds(2f);
        woke.SetActive(false);
        looking.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        looking.SetActive(false);
        awkward.SetActive(true);
         yield return new WaitForSeconds(5f);
        awkward.SetActive(false);
        watching.SetActive(true);
        inTransition = false;
        sleepCD = Random.Range(1, 5);
        asleep = false;
    }

    public void backToWatch(){
        deepStare.SetActive(false);
        watching.SetActive(true);
        staring = false;
    }

    public void deepSleep(){
        sleeping.SetActive(true);
        sleeping.transform.GetChild(4).gameObject.SetActive(false);
        watching.SetActive(false);
        looking.SetActive(false);
        woke.SetActive(false);
        awkward.SetActive(false);
        stare.SetActive(false);
        deepStare.SetActive(false);

    }
}
