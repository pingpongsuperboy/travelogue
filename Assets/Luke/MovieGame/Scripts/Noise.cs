using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Noise : MonoBehaviour
{


    string noisepath = "MovieDialogue";
    public List<string> fileLines = new List<string>();
    public TextAsset noisefile;

    public GameObject noiseText1;
    public GameObject noiseText2;

    public bool decreaseNoise = false;

    int index = 0;


    AudioSource AS;
    // Start is called before the first frame update
    void Start()
    {
        AS = GetComponent<AudioSource>();
        fileLines = new List<string>(noisefile.text.Split('\n'));

    }

    // Update is called once per frame
    void Update()
    {
        if(Camera.main.GetComponent<TheaterCamera>().center){
            AS.volume += 0.1f * Time.deltaTime;
        }

        if(decreaseNoise){
            AS.volume -= 1f * Time.deltaTime;
        }

        if(AS.volume <= 0){
            decreaseNoise = false;
            Camera.main.GetComponent<TheaterCamera>().noNoise = true;
        }

        else{
            Camera.main.GetComponent<TheaterCamera>().noNoise = false;
        }
        // if(AS.volume >= 1.0f){
        //     noiseText1.SetActive(true);
        //     noiseText2.SetActive(true);
        // }
        // else{
        //     noiseText1.SetActive(false);
        //     noiseText2.SetActive(false);           
        // }
    }


}
