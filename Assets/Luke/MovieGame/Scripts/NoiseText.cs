using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class NoiseText : MonoBehaviour
{

    string noisepath = "MovieDialogue";
    public List<string> fileLines = new List<string>();
    public TextAsset noisefile;

    public bool changeText = false;

    public int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        
        fileLines = new List<string>(noisefile.text.Split('\n'));

    }

    // Update is called once per frame
    void Update()
    {


        if(changeText){
            index = index + 1;
            changeText = false;
        }

        if (index < fileLines.Count ){
            GetComponent<TextMesh>().text = fileLines[index];
        }
    }
}
