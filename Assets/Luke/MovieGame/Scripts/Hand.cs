using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Hand : MonoBehaviour
{

    public bool getHand = false;
    public bool finalHand = false;

    public GameObject hand;
    public GameObject hand1;
    public GameObject hand2;
    public GameObject hand3;
    public GameObject hand4;

    public GameObject fhand;
    public GameObject fhand1;
    public GameObject fhand2;
    public GameObject fhand3;
    public GameObject fadeOut;

    public GameObject archie;

    public Transform pos;
    public int pointCount = 0;
    public UnityEvent gotStoneEvents;


    // Start is called before the first frame update
    void Start()
    {


    }

    IEnumerator waitFinal(){
        fadeOut.SetActive(true);
        GetComponent<TheaterCamera>().enabled = false;
        finalHand = false;
        
        yield return new WaitForSeconds(5.0f);
        gotStoneEvents.Invoke();
        jsch.GameManager.instance.ReturnToMenu();
    }

    // Update is called once per frame
    void Update()
    {

        if(GetComponent<TheaterCamera>().center){
            hand.SetActive(false);
            hand1.SetActive(false);
            hand2.SetActive(false);
            hand3.SetActive(false);
            hand4.SetActive(false);
            pointCount = 0;
        }

        if(finalHand){
            GetComponent<TheaterCamera>().enabled = false;
            if (Input.GetMouseButtonDown(0))
            {
                switch(pointCount){
                    case 0:
                        fhand.SetActive(true);
                        pointCount = 1;
                        break;
                    case 1:
                        fhand.SetActive(false);
                        fhand1.SetActive(true);
                        pointCount = 2;
                        break;
                    case 2:
                        fhand1.SetActive(false);
                        fhand2.SetActive(true);
                        pointCount = 3;
                        break;
                    case 3:
                        fhand2.SetActive(false);
                        fhand3.SetActive(true);
                        pointCount = 4;
                        StartCoroutine(waitFinal());
                        break;
                    default:
                        break;
                        
                }
            }
        }

        if(getHand){
            if (Input.GetMouseButtonDown(0))
            {
                switch(pointCount){
                    case 0:
                        hand.SetActive(true);
                        pointCount = 1;
                        break;
                    case 1:
                        hand.SetActive(false);
                        hand1.SetActive(true);
                        pointCount = 2;
                        break;
                    case 2:
                        hand1.SetActive(false);
                        hand2.SetActive(true);
                        pointCount = 3;
                        break;
                    case 3:
                        hand2.SetActive(false);
                        hand3.SetActive(true);
                        pointCount = 4;
                        if(!archie.GetComponent<Audience>().inTransition && !archie.GetComponent<Audience>().staring){
                            GetComponent<TheaterCamera>().poked = true;
                        }
                        break;
                    case 4:
                        hand3.SetActive(false);
                        pointCount = 0;
                        break;
                    default:
                        break;
                        
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                switch(pointCount){
                    case 0:
                        break;
                    case 1:
                        hand.SetActive(false);
                        pointCount = 0;
                        break;
                    case 2:
                        hand1.SetActive(false);
                        hand.SetActive(true);
                        pointCount = 1;
                        break;
                    case 3:
                        hand2.SetActive(false);
                        hand1.SetActive(true);
                        pointCount = 2;
                        break;
                    case 4:
                        hand3.SetActive(false);
                        hand2.SetActive(true);
                        pointCount = 3;
                        break;                        
                    default:
                        break;
                        
                }
            }

            
        }
    }

}
